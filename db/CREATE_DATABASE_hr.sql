CREATE DATABASE hr;

CREATE USER 'hr'@'%' IDENTIFIED WITH mysql_native_password BY 'hr2022';
GRANT ALL PRIVILEGES ON hr.* TO 'hr'@'%';