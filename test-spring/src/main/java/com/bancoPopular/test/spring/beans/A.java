package com.bancoPopular.test.spring.beans;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.bancoPopular.test.spring.noBean.CalculosMatematicos;

@Scope("prototype")
@Component
public class A {
	
	@Value("${numeroDePrueba}")
	private Integer numeroPrueba;
	
	@Value("${test.cadena1}")
	private String cadena1;
	
	@Value("${numeroDeCasos:0}")
	private Integer numeroCasos;
	
	@Value("${test.cadena2:}")
	private String cadena2;
	
	@Value("#{systemEnvironment['USER']}")
	private String username;
	
	@Value("#{systemEnvironment}")
	private Properties se;
	
	@Autowired
	private Environment env;
	
	@Value("#{systemProperties['java.io.tmpdir']}")
	private String tmp;
	
	@Value("#{systemProperties['user.name']}")
	private String user_name;
	
	@Value("#{systemProperties}")
	private Properties sp;
	
	@Value("#{myBeanB.getHello}")
	private String hello;
	
	@Autowired
	private B b;
	
	@Autowired
	@Qualifier("MyBeanD")
	private D d;
	
	@Autowired
	private CalculosMatematicos cm;
	
	public A() {
		System.out.println("Constructor");
	} 
	
	@PostConstruct
	public void init() {
		System.out.println("Método Init");
		System.out.println("numeroPrueba: " + numeroPrueba);
		System.out.println("cadena1: " + cadena1);
		
		String username = env.getProperty("USER");
		System.out.println("username: " + username);
	}

	public void print() {
		System.out.println("Imprimiendo desde A");
		b.print();
		d.print();
	}

	public B getB() {
		return b;
	}

	//@Autowired
	public void setB(B b) {
		this.b = b;
	}
	
	public void sumarDosNumeros(double num1, double num2) {
		//CalculosMatematicos cm = new CalculosMatematicos();
		
		System.out.println("La suma es: " + cm.sumar(num1, num2));
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("Método Destroy");
	}
}
