package com.bancoPopular.test.spring.constants;

public enum MathConstants {

	PHI (3.1416),
	E (2.8182);
	
	private double value;
	
	private MathConstants(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}
	
	
	
}
