package com.bancoPopular.test.spring.service;

import org.springframework.stereotype.Service;

@Service("MyCalcularRadianes")
public class CalcularRadianes implements ICalcular {

	public double calcularAngulo(double x1, double y1, double x2, double y2) {
		return Math.PI;
	}
	
	public double getPI() {
		return Math.PI;
	}

}
