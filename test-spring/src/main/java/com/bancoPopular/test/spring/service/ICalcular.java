package com.bancoPopular.test.spring.service;

import org.springframework.stereotype.Service;

@Service
public interface ICalcular {

	public static final double PHI = 3.1416;
	
	public double calcularAngulo(double x1, double y1, double x2, double y2);
	
}
