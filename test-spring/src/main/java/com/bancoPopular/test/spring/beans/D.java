package com.bancoPopular.test.spring.beans;

import org.springframework.stereotype.Component;

@Component("MyBeanD")
public class D {

	public void print() {
		System.out.println("Imprimiendo desde D");
	}
	
}
