package com.bancoPopular.test.spring.noBean;

public class CalculosMatematicos {

	public double sumar(double num1, double num2) {
		return num1 + num2;
	}
	
	public double restar(double num1, double num2) {
		return num1 - num2;
	}
}
