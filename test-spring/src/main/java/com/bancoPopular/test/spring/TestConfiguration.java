package com.bancoPopular.test.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.bancoPopular.test.spring.beans.A;
import com.bancoPopular.test.spring.conf.Test2Configuration;

@Configuration
@ComponentScan(basePackages = {"com.bancoPopular.test.spring"})
@PropertySource("classpath:MyProperties.properties")
@Import({Test2Configuration.class})
public class TestConfiguration {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = 
				new AnnotationConfigApplicationContext(TestConfiguration.class);
		
				
		A a = ctx.getBean(A.class);
		a.print();
		a.sumarDosNumeros(3, 5);
		
		/*A a1 = ctx.getBean(A.class);
		a1.print();*/
		
		/*B b = ctx.getBean(B.class);
		b.print();
		b.print2();*/
		
		/*A a = new A();
		a.print();*/
		
		/*D d = ctx.getBean(D.class);
		d.print();
		
		D d1 = (D) ctx.getBean("MyBeanD");
		d1.print();*/
		
		//ICalcular calcular = new CalcularGrados();
		//ICalcular calcular = ctx.getBean(ICalcular.class);
		//ICalcular calcular = (ICalcular) ctx.getBean("MyCalcularGrados");
		/*double anguloGrados = calcular.calcularAngulo(0, 0, 0, 0);
		System.out.println("El angulo en grados es: " + anguloGrados);
		
		calcular = (ICalcular) ctx.getBean("MyCalcularRadianes");
		double anguloRad = calcular.calcularAngulo(0, 0, 0, 0);
		System.out.println("El angulo en radianes es: " + anguloRad);*/
	}

}
