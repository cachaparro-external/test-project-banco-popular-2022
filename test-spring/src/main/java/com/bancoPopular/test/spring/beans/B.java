package com.bancoPopular.test.spring.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("myBeanB")
public class B {
	
	private C c;
	
	@Autowired
	public B(C c) {
		this.c = c;
	}
	
	public void print() {
		System.out.println("Imprimiendo desde B");
		c.print();
	}
	
	public void print2() {
		System.out.println("Imprimiendo2 desde B");
	}
	
	public void print3() {
		System.out.println("Imprimiendo3 desde B");
	}
	
	public String getHello() {
		return "Hello World!!!";
	}
	
}
