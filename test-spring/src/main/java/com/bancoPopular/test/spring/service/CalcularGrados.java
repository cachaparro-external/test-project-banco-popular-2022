package com.bancoPopular.test.spring.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service("MyCalcularGrados")
@Primary
public class CalcularGrados implements ICalcular {

	public double calcularAngulo(double x1, double y1, double x2, double y2) {
		return 0;
	}
	
	public int getOrigen() {
		return 0;
	}

}
