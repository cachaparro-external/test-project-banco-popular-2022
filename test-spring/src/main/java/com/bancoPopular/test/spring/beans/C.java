package com.bancoPopular.test.spring.beans;

import org.springframework.stereotype.Component;

@Component
public class C {

	public void print() {
		System.out.println("Imprimiendo desde C");
	}
	
}
