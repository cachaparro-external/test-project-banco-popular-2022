package com.bancoPopular.test.spring.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bancoPopular.test.spring.noBean.CalculosMatematicos;

@Configuration
public class Test2Configuration {

	@Bean("MyCalculos")
	//@Bean()
	public CalculosMatematicos getCalculosMatematicos() {
		CalculosMatematicos cm = new CalculosMatematicos();
		
		return cm;
	}
	
	
}
