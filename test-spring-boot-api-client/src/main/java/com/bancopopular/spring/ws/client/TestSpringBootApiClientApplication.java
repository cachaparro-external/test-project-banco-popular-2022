package com.bancopopular.spring.ws.client;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.bancopopular.spring.ws.client.dto.EstudianteDTO;
import com.bancopopular.spring.ws.client.integration.rest.EstudianteAPIClient;

@SpringBootApplication
public class TestSpringBootApiClientApplication {

	@Autowired
	private EstudianteAPIClient client;
	
	public static void main(String[] args) {
		SpringApplication.run(TestSpringBootApiClientApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			/*EstudianteDTO dto = client.invokeFindById(30L);*/
			
			EstudianteDTO dto = client.invokeFindByIdExchange(30L);
			
			if(dto != null) {
				System.out.println("Datos del estudiante: " + dto);
			}else {
				System.out.println("No existe el estudiante");
			}
			
			/*Optional<EstudianteDTO> estudianteOpt = client.invokeFindById2(300L);
			
			if(estudianteOpt.isPresent()) {
				System.out.println("Datos del estudiante: " + estudianteOpt.get());
			}else {
				System.out.println("No existe el estudiante");
			}*/
			
			//List<EstudianteDTO> lista = this.client.invokeFindByFirstAndLastName("Cesar", "Pedraza");
			//List<EstudianteDTO> lista = this.client.invokeFindByFirstAndLastName2("Cesar", "Pedraza");
			
			/*lista.forEach((estudiante) -> {
				System.out.println("Estudiante: " + estudiante);
			});*/
			
			//lista.forEach(estudiante -> System.out.println("Estudiante: " + estudiante));
			
			/*lista.forEach(System.out::print);*/
			
			/*EstudianteDTO dto = new EstudianteDTO();
			dto.setFirstName("Manuel");
			dto.setLastName("Perdomo");
			dto.setBirthDate(LocalDate.now());*/
			
			/*
			//Long idEstudiante = this.client.invokeCreate(dto);
			Long idEstudiante = this.client.invokeCreateExchange(dto);
			
			if(idEstudiante != null) {
				System.out.println("Se creo el estudiante con ID: " + idEstudiante);
			}else {
				System.err.println("Ya existe el estudiante");
			}*/
			
			//dto.setId(20L);
			//this.client.invokeModify2(dto);
			
			//this.client.invokeDelete(25L);
			
			//this.client.invokeEjecutarNomina("cachaparro");
			
			//this.client.invokeEjecutarNomina2();
		};
	}
	
	//29/11/2022
	//1. findByFirstAndLastName2
	//2. getAll
	//3. findByFirstAndLastName4
	//4. create2
	//5. modify
	
	//Ver:
	//1. POST con Exchange
	//2. Lista con Collections
	//
	

}
