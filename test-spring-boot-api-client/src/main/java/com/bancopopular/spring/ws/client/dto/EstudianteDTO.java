package com.bancopopular.spring.ws.client.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class EstudianteDTO {

	private Long id;
	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	private String email;
	
}
