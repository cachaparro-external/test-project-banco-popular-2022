package com.bancopopular.spring.ws.client.config;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
	
	@Value("${username_api}")
	private String usernameApi;
	
	@Value("${password_api}")
	private String passwordApi;

	@Bean
	public RestTemplate getRestTemplate(RestTemplateBuilder rtb) {
		RestTemplate rt = 
				rtb
				.basicAuthentication(usernameApi, passwordApi)
				.setConnectTimeout(Duration.ofSeconds(30))
				.setReadTimeout(Duration.ofSeconds(60))
				.build();
		
		return rt;
	}
	
}
