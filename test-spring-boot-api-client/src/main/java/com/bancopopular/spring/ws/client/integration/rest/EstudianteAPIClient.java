package com.bancopopular.spring.ws.client.integration.rest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.bancopopular.spring.ws.client.dto.EstudianteDTO;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EstudianteAPIClient {

	@Value("${estudiante.mainUrl}")
	private String mainUrl;
	
	@Autowired
	private RestTemplate rt;
	
	public EstudianteDTO invokeFindById(Long idEstudiante) {
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/id/{idEstudiante}");
			
			Map<String, Object> params = new HashMap<>();
			params.put("idEstudiante", idEstudiante);
			
			ResponseEntity<EstudianteDTO> response = rt.getForEntity(uriBuilder.build(params), EstudianteDTO.class);
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				return response.getBody();
			}else if(response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
				return null;
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeFindById para ID {}", idEstudiante);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public EstudianteDTO invokeFindByIdExchange(Long idEstudiante) {
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/id/{idEstudiante}");
			
			Map<String, Object> params = new HashMap<>();
			params.put("idEstudiante", idEstudiante);
			
			ResponseEntity<EstudianteDTO> response = rt.exchange(uriBuilder.build(params), HttpMethod.GET, null, EstudianteDTO.class);
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				return response.getBody();
			}else if(response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
				return null;
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeFindById para ID {}", idEstudiante);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public Optional<EstudianteDTO> invokeFindById2(Long idEstudiante) {
		try{
			Map<String, Object> params = new HashMap<>();
			params.put("idEstudiante", idEstudiante);
			
			//Siempre usar UriBuilder
			ResponseEntity<EstudianteDTO> response = rt.getForEntity(mainUrl + "/id/{idEstudiante}", EstudianteDTO.class, params);
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				return Optional.of(response.getBody());
			}else if(response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
				return Optional.empty();
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeFindById2 para ID {}", idEstudiante);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public List<EstudianteDTO> invokeFindByFirstAndLastName(String firstName, String lastName) {
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/firstName/{firstName}/lastName/{lastName}");
			
			Map<String, Object> params = new HashMap<>();
			params.put("firstName", firstName);
			params.put("lastName", lastName);
			
			ResponseEntity<EstudianteDTO[]> response = rt.getForEntity(uriBuilder.build(params), EstudianteDTO[].class);
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				List<EstudianteDTO> lista = Arrays.asList(response.getBody());
				
				return lista;
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeFindByFirstAndLastName para fn {} ln {}", firstName, lastName);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public List<EstudianteDTO> invokeFindByFirstAndLastName2(String firstName, String lastName) {
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.queryParam("firstName", firstName)
						.queryParam("lastName", lastName);
			
			ResponseEntity<EstudianteDTO[]> response = rt.getForEntity(uriBuilder.build(), EstudianteDTO[].class);
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				List<EstudianteDTO> lista = Arrays.asList(response.getBody());
				
				return lista;
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeFindByFirstAndLastName para fn {} ln {}", firstName, lastName);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public Long invokeCreate(EstudianteDTO dto) {
		Assert.notNull(dto, "El DTO es nulo");
		Assert.hasText(dto.getFirstName(), "FN es nulo o vacío");
		Assert.hasText(dto.getLastName(), "LN es nulo o vacío");
		
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl);
			
			ResponseEntity<Long> response = rt.postForEntity(uriBuilder.build(), dto, Long.class);
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				return response.getBody();
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		//Por los 4XX
		}catch (HttpClientErrorException e) {
			if(e.getStatusCode().equals(HttpStatus.CONFLICT)){
				return null;
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", e.getRawStatusCode()));
			}
		}
	}
	
	public Long invokeCreateExchange(EstudianteDTO dto) {
		Assert.notNull(dto, "El DTO es nulo");
		Assert.hasText(dto.getFirstName(), "FN es nulo o vacío");
		Assert.hasText(dto.getLastName(), "LN es nulo o vacío");
		
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl);
			
			MultiValueMap<String, String> headers = new HttpHeaders();
			headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			
			//HttpEntity<EstudianteDTO> request = new HttpEntity<EstudianteDTO>(dto);
			HttpEntity<EstudianteDTO> request = new HttpEntity<EstudianteDTO>(dto, headers);
			
			ResponseEntity<Long> response = rt.exchange(uriBuilder.build(), HttpMethod.POST, request, Long.class); 
			
			//Por los 2XX
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				return response.getBody();
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		//Por los 4XX
		}catch (HttpClientErrorException e) {
			if(e.getStatusCode().equals(HttpStatus.CONFLICT)){
				return null;
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", e.getRawStatusCode()));
			}
		}
	}
	
	public void invokeModify2(EstudianteDTO dto) {
		Assert.notNull(dto, "El DTO es nulo");
		Assert.notNull(dto.getId(), "ID es nulo");
		Assert.hasText(dto.getFirstName(), "FN es nulo o vacío");
		Assert.hasText(dto.getLastName(), "LN es nulo o vacío");
		
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/id/{id}");
			
			Map<String, Object> params = new HashMap<>();
			params.put("id", dto.getId());
			
			dto.setId(null);			
			
			//rt.put(uriBuilder.build(params), dto);
			
			HttpEntity<EstudianteDTO> request = new HttpEntity<EstudianteDTO>(dto);
			
			ResponseEntity<Void> response = rt.exchange(uriBuilder.build(params), HttpMethod.PUT, request, Void.class);
			
			if(!response.getStatusCode().equals(HttpStatus.OK)) {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		//Por los 4XX
		}catch (HttpClientErrorException e) {
			log.error("Error: ", e);
			
			throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", e.getRawStatusCode()));
		}catch (HttpServerErrorException e) {
			log.error("Error: ", e);
			
			throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", e.getRawStatusCode()));
		}
	}
	
	public void invokeDelete(Long idEstudiante) {
		Assert.notNull(idEstudiante, "El ID es nulo");
		
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/id/{id}");
			
			Map<String, Object> params = new HashMap<>();
			params.put("id", idEstudiante);
			
			//rt.delete(uriBuilder.build(params));
			
			ResponseEntity<Void> response = rt.exchange(uriBuilder.build(params), HttpMethod.DELETE, null, Void.class);
			
			if(!response.getStatusCode().equals(HttpStatus.OK)) {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeDelete para ID {}", idEstudiante);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public void invokeEjecutarNomina(String username) {
		Assert.hasText(username, "El username es nulo o vacío");
		
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/execute");
			
			MultiValueMap<String, String> headers = new HttpHeaders();
			headers.add("username", username);
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
			
			HttpEntity<Void> request = new HttpEntity<>(headers);
			
			ResponseEntity<Void> response = rt.exchange(uriBuilder.build(), HttpMethod.POST, request, Void.class);
			
			if(!response.getStatusCode().equals(HttpStatus.OK)) {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeEjecutarNomina para username {}", username);
			log.error("Error: ", e);
			
			throw e;
		}
	}
	
	public void invokeEjecutarNomina2() {
		try{
			UriBuilder uriBuilder = 
					UriComponentsBuilder
						.fromHttpUrl(mainUrl)
						.path("/execute/2");
			
			ResponseEntity<Void> response = rt.exchange(uriBuilder.build(), HttpMethod.POST, null, Void.class);
			
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				response.getHeaders().forEach((key, value) -> {
					System.out.println("Header: " + key + ": " + value);
				});
			}else {
				throw new IllegalStateException(String.format("Invalid HTTP Responde Code: %d", response.getStatusCodeValue()));
			}
		}catch (Exception e) {
			log.error("Error en invokeEjecutarNomina2");
			log.error("Error: ", e);
			
			throw e;
		}
	}
}
