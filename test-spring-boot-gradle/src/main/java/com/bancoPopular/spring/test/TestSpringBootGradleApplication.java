package com.bancoPopular.spring.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpringBootGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSpringBootGradleApplication.class, args);
		
		System.out.println("Hello world ffrom Gradle");
	}

}
