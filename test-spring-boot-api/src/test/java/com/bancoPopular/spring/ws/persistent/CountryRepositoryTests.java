package com.bancoPopular.spring.ws.persistent;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.Assert;

import com.bancoPopular.spring.ws.persistent.entity.CountryEntity;
import com.bancoPopular.spring.ws.persistent.repository.CountryRepository;

import lombok.extern.log4j.Log4j2;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@Log4j2
public class CountryRepositoryTests {
	
	@Value("${regionName.europe}")
	private String regionName;
	
	@Value("${regionName.noExists}")
	private String regionNameNotExists;

	@Autowired
	private CountryRepository repository;
	
	@Test
	@DisplayName("Prueba que verifica si la consulta retorna información para una región válida")
	public void buscarPaisPorNombreRegionTest() {
		List<CountryEntity> countries = this.repository.buscarPaisPorNombreRegion(regionName);
		
		assert countries != null;
		assert countries.size() > 0;
		
		log.info("Respuesta de buscarPaisPorNombreRegion: {}", countries);
	}	
	
	@Test
	@DisplayName("Prueba que verifica si la consulta NO retorna información")
	public void buscarPaisPorNombreRegionVacioTest() {
		List<CountryEntity> countries = this.repository.buscarPaisPorNombreRegion(regionNameNotExists);
		
		assert countries != null;
		assert countries.size() == 0;
	}	
	
	@Test
	@DisplayName("Prueba de buscarPaisPorNombreRegionMejorada que retorna información")
	public void buscarPaisPorNombreRegionMejoradaTest() {
		List<CountryEntity> countries = this.repository.buscarPaisPorNombreRegionMejorada(regionName);
		
		Assert.notEmpty(countries, "La consulta retorno nulo o vacío");
		
		log.info("Respuesta de buscarPaisPorNombreRegionMejorada: {}", countries);
	}
	
	@Test
	@DisplayName("Prueba de buscarPaisPorNombreRegionMejorada que NO retorna información")
	public void buscarPaisPorNombreRegionMejoradaBaviaTest() {
		List<CountryEntity> countries = this.repository.buscarPaisPorNombreRegionMejorada(regionNameNotExists);
		
		Assert.notNull(countries, "La lista de resultados en NULA");
		Assert.isTrue(countries.size() == 0, "La consulta retorno resultados");
	}
	
	//22/11/2022
	//1. Crear las pruebas automaticas (JUnit) para el repositorio de Empleados:
	// a. Consultar por ID (Debe retornar información, 1 registro) --> Método CRUDRepository
	// b. Consultar por nombre y apellido (Debe retornar información)  --> Método 
	//2. Para Country
	// a. Consultar todos los paises por Region Name (La respuesta no sea nula)
	// b. Crear país (Creación exitosa y País ya existe)
	// c. Actualizar nombre del país (Actualización exitosa)
}
