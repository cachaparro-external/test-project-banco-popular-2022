package com.bancoPopular.spring.ws.persistent;

import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.bancoPopular.spring.ws.dto.RegionDTO;
import com.bancoPopular.spring.ws.persistent.dao.RegionDAO;

import lombok.extern.log4j.Log4j2;

@SpringBootTest
@Log4j2
public class RegionDAOTests {

	@Autowired
	private RegionDAO dao;
	
	@Test
	public void getAllTest() {
		List<RegionDTO> regions = this.dao.getAll();
		
		Assert.notNull(regions, "Regions es nulo");
		
		log.info("Regions: {}", regions);
	}
	
	@Test
	@Disabled
	public void getAllIdBetweenTest() {
		Random random = new Random(System.currentTimeMillis());
		
		List<RegionDTO> regions = this.dao.getAllIdBetween((long)random.nextInt(10), (long)random.nextInt(100));
		
		Assert.notNull(regions, "Regions es nulo");
		
		log.info("Regions: {}", regions);
	}
	
}
