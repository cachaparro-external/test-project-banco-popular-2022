package com.bancoPopular.spring.ws.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.Assert;

import com.bancoPopular.spring.ws.dto.EstudianteDTO;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.internal.mapping.Jackson2Mapper;
import io.restassured.mapper.ObjectMapper;
import io.restassured.path.json.mapper.factory.DefaultJackson2ObjectMapperFactory;
import io.restassured.path.json.mapper.factory.Jackson2ObjectMapperFactory;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@TestPropertySource("classpath:test.properties")
@Log4j2
public class EstudianteAPITests {
	
	private final int MAX_ESTUDIANTE_ID = 50; 
	
	@Value("${main.url}")
	private String mainUrl;
	
	@Value("${consultaId.url}")
	private String consultaIdUrl;
	
	@Value("${consultaNames.url}")
	private String consultaNamesUrl;
	
	@Value("${username}")
	private String username;
	
	@Value("${username_api}")
	private String usernameApi;
	
	@Value("${password_api}")
	private String passwordApi;
	
	private Random random;
	
	@PostConstruct
	public void init() {
		random = new Random(System.currentTimeMillis());
	}
	
	@BeforeAll
	public static void antesDeTodasLasPruebas() {
		System.out.println("@BeforeAll");
	}
	
	@BeforeEach
	public void antesDeCadaPrueba() {
		System.out.println("@BeforeEach");
	}

	@Test
	@Disabled
	public void test() {
		RestAssured
			.given()//Request start
			.log().all() //Imprima en el log todo lo relacionado con el request
			.and()
			.when()//Request end
			.get()//Invocación del API
			.then()//Response end
			.log().all() //Imprima en el log todo lo relacionado con el response
			.statusCode(HttpStatus.OK.value());//Response end
	}
	
	@Test
	public void findByIdOkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.auth()
			.basic(usernameApi, passwordApi)
			.and()
			.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
			.when()
			.get(consultaIdUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value())
			.body("firstName", equalTo("Jose"))
			.body("lastName", equalTo("Perez"))
			.body("birthDate", not(equalTo("2022-11-29")));
	}
	
	@Test
	public void findByIdUnAuthorizedTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			//.auth()
			//.basic(usernameApi, passwordApi)
			.and()
			.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
			.when()
			.get(consultaIdUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.UNAUTHORIZED.value());
	}
	
	@Test
	public void findByIdUnAuthorized2Test() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.auth()
			.basic("usuariox", "999999999")
			.and()
			.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
			.when()
			.get(consultaIdUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.UNAUTHORIZED.value());
	}
	
	@Test
	public void findByIdOk2Test() {
		ExtractableResponse<Response> response = 
			RestAssured
				.given()
				.log().all() 
				.and()
				.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
				.when()
				.get(consultaIdUrl)
				.then()
				.log().all() 
				.statusCode(HttpStatus.OK.value())
				.extract();
			
			/*Jackson2ObjectMapperFactory factory = new DefaultJackson2ObjectMapperFactory();
			
			io.restassured.mapper.ObjectMapper om = new Jackson2Mapper(factory);
			
			EstudianteDTO responseDto = response.as(EstudianteDTO.class, om);*/
			
			log.info("JSON: " + response.asString());
			log.info("Content Type: " + response.contentType());
			
			String fn = response.jsonPath().getString("firstName");
			
			log.info("fn: " + fn);
			
			assertEquals("Jose", fn);
	}
	
	@Test
	public void findByIdOk3Test() {
		ExtractableResponse<Response> response = 
			RestAssured
				.given()
				.log().all() 
				.and()
				.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
				.when()
				.get(consultaIdUrl)
				.then()
				.log().all() 
				.statusCode(HttpStatus.OK.value())
				.extract();
			
			Jackson2ObjectMapperFactory factory = new DefaultJackson2ObjectMapperFactory();
			
			ObjectMapper om = new Jackson2Mapper(factory);
			
			EstudianteDTO responseDto = response.as(EstudianteDTO.class, om);
			
			assertEquals("Jose", responseDto.getFirstName());
	}
	
	@Test
	public void findByIdNoContentTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.pathParam("idEstudiante", MAX_ESTUDIANTE_ID + random.nextInt(MAX_ESTUDIANTE_ID))
			.when()
			.get(consultaIdUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.NO_CONTENT.value());
	}
	
	@Test
	public void findByFirstAndLastNameOkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.pathParam("firstName", "Jorge")
			.pathParam("lastName", "Aponte")
			.when()
			.get(consultaNamesUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value())
			.body("", not(anyOf(nullValue())));
	}
	
	@Test
	public void findByFirstAndLastName3OkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.queryParam("firstName", "Jorge")		
			.queryParam("lastName", "Aponte")
			.when()
			.get(mainUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void findByFirstAndLastName3BadRequestTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			//.queryParam("firstName", "Jorge")		
			.queryParam("lastName", "Aponte")
			.when()
			.get(mainUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void getAllOkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.when()
			.get(mainUrl + "/all")
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void findByFirstAndLastName4OkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.contentType(ContentType.JSON)
			.body(this.getEstudianteDTO())
			.when()
			.post(mainUrl + "/byFilter")
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void createOkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.contentType(ContentType.JSON)
			.body(this.getEstudianteDTO())
			.when()
			.post(mainUrl)
			.then()
			.log().all() 
			.statusCode(anyOf(is(HttpStatus.OK.value()), is(HttpStatus.CONFLICT.value())));
	}
	
	@Test
	public void modify2OkTest() {
		EstudianteDTO dto = this.getEstudianteDTO();
		dto.setId(null);
		
		RestAssured
			.given()
			.log().all() 
			.and()
			.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
			.and()
			.contentType(ContentType.JSON)
			.body(dto)
			.when()
			.put(consultaIdUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void deleteOkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
			.when()
			.delete(consultaIdUrl)
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void ejecutarNominaOkTest() {
		Header usernameHeader = new Header("username", username);
		
		RestAssured
			.given()
			.log().all() 
			.and()
			.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.header(usernameHeader)
			.when()
			.post(mainUrl + "/execute")
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void ejecutarNomina2OkTest() {
		ExtractableResponse<Response> response =
			RestAssured
				.given()
				.log().all() 
				.and()
				.when()
				.post(mainUrl + "/execute/2")
				.then()
				.log().all() 
				.statusCode(HttpStatus.OK.value())
				.extract();
		
		String username = response.header("username");
		String contentType = response.contentType();
		
		Long time1 = response.time();
		Long time2 = response.timeIn(TimeUnit.SECONDS);
		
		log.info("Username: " + username);		
		log.info("contentType: " + contentType);
		log.info("Time: " + time1 + " - " + time2);	
		
		Assert.hasText(username, "USername es nulo o vacío");
		assertEquals("cachaparro", username);
		assertThat(time1 <= 1000);
	}
	
	@Test
	public void ejecutarNomina3OkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.when()
			.post(mainUrl + "/execute/2")
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value())
			.header("username", equalTo("cachaparro"))
			.time(lessThan(800L))
			.time(greaterThan(200L));
			//.time(lessThan(1L), TimeUnit.SECONDS);
	}
	
	private EstudianteDTO getEstudianteDTO() {
		EstudianteDTO dto = new EstudianteDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName("Jaime");
		dto.setLastName("Garcia");
		dto.setId(random.nextLong());
		
		return dto;
	}
	
	@AfterAll
	public static void despuesDeTodasLasPruebas() {
		System.out.println("@AfterAll");
	}
	
	@AfterEach
	public void despuesDeCadaPrueba() {
		System.out.println("@@AfterEach");
	}
	
	//24/11/2022
	//1. Test para findByFirstAndLastName2, create2, modify, findByFirstAndLastName4
	//2. Country agregar todas las pruebas
	
}
