package com.bancoPopular.spring.ws.business;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.Assert;

import com.bancoPopular.spring.ws.dto.CountryDTO;
import com.bancoPopular.spring.ws.persistent.entity.CountryEntity;
import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;
import com.bancoPopular.spring.ws.persistent.repository.CountryRepository;
import com.bancoPopular.spring.ws.persistent.repository.RegionRepository;
import com.bancoPopular.spring.ws.util.exception.EntityAlreadyExistsException;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class CountryServiceTests {
	
	@Value("${country.id}")
	private String countryId;
	
	@Value("${regionName.europe}")
	private String regionName;
	
	@Value("${regionName.noExists}")
	private String regionNameNotExists;
	
	@Autowired
	private CountryService service;
	
	@Autowired
	private CountryRepository repository;
	
	@Autowired
	private RegionRepository regionRepository;
	
	@Test
	@DisplayName("Creación de país que ya existe por ID")
	public void createCountryAldeadyExists() {
		assertThrows(EntityAlreadyExistsException.class, () -> {
			CountryDTO dto = this.getCountryDTO();
			dto.setId(countryId);
			
			this.service.create(dto);
		});
	}
	
	@Test
	@DisplayName("Creación de país que cuya región no existe")
	public void createRegionNoExists() {
		assertThrows(IllegalArgumentException.class, () -> {
			CountryDTO dto = this.getCountryDTO();
			dto.setRegionName(regionNameNotExists);
			
			this.service.create(dto);
		});
	}
	
	@Test
	@DisplayName("Creación de país exitosa")
	public void createSuccess() {
		assertDoesNotThrow(() -> {
			CountryDTO dto = this.getCountryDTO();
			
			String countryIdRet = this.service.create(dto);
			
			Assert.hasText(countryIdRet, "El ID es nulo o vacío");
			
			//Assert.isTrue(dto.getId().equals(countryIdRet), "El ID es diferente");
			assertEquals(dto.getId(), countryIdRet, "El ID es diferente");
		});
	}
	
	@Test
	@DisplayName("Creación de país exitosa con enfoque de dejar el sistema en las mismas condiciones")
	public void createSuccess2() {
		CountryDTO dto = this.getCountryDTO();
		
		Optional<CountryEntity> entityOpt = this.repository.findById(dto.getId());
		
		try{
			if(entityOpt.isPresent()) {
				this.repository.delete(entityOpt.get());
			}
			
			assertDoesNotThrow(() -> {
				String countryIdRet = this.service.create(dto);
				
				Assert.hasText(countryIdRet, "El ID es nulo o vacío");
				
				//Assert.isTrue(dto.getId().equals(countryIdRet), "El ID es diferente");
				assertEquals(dto.getId(), countryIdRet, "El ID es diferente");
				
				this.repository.deleteById(countryIdRet);
			});
		}catch (Exception e) {
			throw e;
		}finally {
			if(entityOpt.isPresent()) {
				this.repository.save(entityOpt.get());
			}
		}
	}
	
	@Test
	@DisplayName("Modificación de país exitosa con enfoque de dejar el sistema en las mismas condiciones")
	public void createModify2() {
		CountryDTO dto = this.getCountryDTO();
		
		Optional<CountryEntity> entityOpt = this.repository.findById(dto.getId());
		
		try{
			if(entityOpt.isEmpty()) {
				CountryEntity entity = new CountryEntity();
				entity.setId(dto.getId());
				entity.setName(dto.getName());
				
				Optional<RegionEntity> regionOpt = this.regionRepository.findByName(dto.getRegionName());
				
				if(regionOpt.isPresent()) {
					entity.setRegion(regionOpt.get());
				}else {
					assert false;
				}	
				
				entity = this.repository.save(entity);
			}
			
			assertDoesNotThrow(() -> {
				this.service.modify(dto);
			});
		}catch (Exception e) {
			throw e;
		}finally {
			if(entityOpt.isEmpty()) {
				this.repository.deleteById(dto.getId());
			}
		}
	}
	
	/*@Test
	@DisplayName("Creación de país con nombre de país nulo")
	public void createCountryNameNull() {
		assertThrows(Exception.class, () -> {
			CountryDTO dto = this.getCountryDTO();
			dto.setName(null);
			
			this.service.create(dto);
		});
	}*/
	
	private CountryDTO getCountryDTO() {
		String countryId = RandomStringUtils.random(2, true, false);
		
		CountryDTO dto = new CountryDTO();
		dto.setId(countryId.toUpperCase());
		dto.setName("País de prueba");
		dto.setRegionName(regionName);
		
		return dto;
	}
	
	//23/11/2022
	//1. Crear las pruebas automaticas (JUnit) para la capa de negocio de Empleados:
	// a. Consultar por ID (Debe retornar información, 1 registro)
	// b. Consultar por nombre y apellido (Debe retornar información)  --> Método 
	//2. Para Region
	// a. Consultar todos las regiones por Region Name (La respuesta no sea nula)
	// b. Crear region (Creación exitosa y Region por nombre ya existe)
	// c. Actualizar nombre del region (Actualización exitosa)

}
