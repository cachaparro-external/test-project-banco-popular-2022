package com.bancoPopular.spring.ws.api;

import java.time.LocalDate;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import com.bancoPopular.spring.ws.dto.EstudianteDTO;

import io.restassured.RestAssured;
import lombok.extern.log4j.Log4j2;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@TestPropertySource("classpath:test.properties")
@Log4j2
public class EstudianteAPITests2 {
	
	private final int MAX_ESTUDIANTE_ID = 50; 
	
	@Value("${main.url}")
	private String mainUrl;
	
	@Value("${consultaId.url}")
	private String consultaIdUrl;
	
	@Value("${consultaNames.url}")
	private String consultaNamesUrl;
	
	@Value("${username}")
	private String username;
	
	private Random random;
	
	@PostConstruct
	public void init() {
		random = new Random(System.currentTimeMillis());
	}
	
	@BeforeAll
	public static void antesDeTodasLasPruebas() {
		System.out.println("@BeforeAll");
		RestAssured.baseURI = "http://127.0.0.1";
		RestAssured.port = 8181;
		RestAssured.basePath = "/api/banco-popular/estudiante";
	}
	
	@BeforeEach
	public void antesDeCadaPrueba() {
		System.out.println("@BeforeEach");
	}

	
	
	@Test
	public void findByIdOkTest() {
		RestAssured
			.given()
			.log().all() 
			.and()
			.pathParam("idEstudiante", random.nextInt(MAX_ESTUDIANTE_ID))
			.when()
			.get("/id/{idEstudiante}")
			.then()
			.log().all() 
			.statusCode(HttpStatus.OK.value());
	}
	
	
	
	private EstudianteDTO getEstudianteDTO() {
		EstudianteDTO dto = new EstudianteDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName("Jaime");
		dto.setLastName("Garcia");
		dto.setId(random.nextLong());
		
		return dto;
	}
	
	@AfterAll
	public static void despuesDeTodasLasPruebas() {
		System.out.println("@AfterAll");
		RestAssured.reset();
	}
	
	@AfterEach
	public void despuesDeCadaPrueba() {
		System.out.println("@AfterEach");
	}
	
	//4. URL por defecto
	//5. Como enviar parametros de seguridad
	
	//24/11/2022
	//1. Test para findByFirstAndLastName2, create2, modify, findByFirstAndLastName4
	//2. Country agregar todas las pruebas
	
}
