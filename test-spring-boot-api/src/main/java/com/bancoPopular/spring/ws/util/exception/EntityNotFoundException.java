package com.bancoPopular.spring.ws.util.exception;

public class EntityNotFoundException extends RuntimeException{

	public EntityNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EntityNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EntityNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
