package com.bancoPopular.spring.ws.persistent.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bancoPopular.spring.ws.persistent.entity.EmployeeEntity;

@Repository
public interface Employee3Repository extends PagingAndSortingRepository<EmployeeEntity, Long>{
	
	public Page<EmployeeEntity> findByDepartmentId(Long departmentId, Pageable pageable);
	
	//public Page<EmployeeEntity> findByDepartmentId(Long departmentId, Pageable pageable, Sort sort);
	
	@Query(nativeQuery = false,
			value = "SELECT e "
					+ "FROM EmployeeEntity e "
					+ "WHERE e.salary > :salary")
	public Page<EmployeeEntity> buscarPorSalarioMayor(@Param("salary") Double salary, Pageable pageable);
	
	@Query(nativeQuery = true,
			value = "SELECT * "
					+ "FROM employees e "
					+ "WHERE e.salary > :salary")
	public Page<EmployeeEntity> buscarPorSalarioMayor2(@Param("salary") Double salary, Pageable pageable);
	
	public List<EmployeeEntity> findByHireDate(LocalDate hireDate);
	
	public List<EmployeeEntity> findBySalaryLessThanAndCommissionPctIsNullAndJobIdIn(Double salary, List<String> jobIds, Sort sort);
	
	public List<EmployeeEntity> findBySalaryLessThanAndCommissionPctIsNullAndJobIdIn(Double salary, List<String> jobIds, Pageable pageable);

}
