package com.bancoPopular.spring.ws.dto;

import org.dozer.Mapping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestADTO {
	
	//@Mapping("idTest")
	private Long id;
	
	//@Mapping("nameTest")
	private String name;
	
	private A1 a1;

}
