package com.bancoPopular.spring.ws.dto;

import lombok.Data;

@Data
public class FilterDTO {

	private Long min; 
	private Long max;
	
}
