package com.bancoPopular.spring.ws.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "countries")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryEntity {

	@Id
	@Column(name = "country_id")
	private String id;
	
	@Column(name = "country_name")
	private String name;
	
	/*@Column(name = "region_id")
	private Long regionId;*/
	
	@ManyToOne(targetEntity = RegionEntity.class, optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "region_id", referencedColumnName = "region_id")
	private RegionEntity region;
}
