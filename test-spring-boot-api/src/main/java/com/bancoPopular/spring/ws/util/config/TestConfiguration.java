package com.bancoPopular.spring.ws.util.config;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

@Configuration
public class TestConfiguration {
	
	@Bean
	public DozerBeanMapper getDozerBeanMapper() {
		List<String> mappings = new ArrayList<>();
		mappings.add("mapping/mappings.xml");
				
		return new DozerBeanMapper(mappings);
	}
	
	@Bean
	public BasicAuthenticationEntryPoint getBasicAuthenticationEntryPoint() {
		BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
		entryPoint.setRealmName("MyRealmName");
		entryPoint.afterPropertiesSet();
		
		return entryPoint;
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	

}
