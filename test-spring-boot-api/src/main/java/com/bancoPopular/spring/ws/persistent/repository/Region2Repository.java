package com.bancoPopular.spring.ws.persistent.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;

public interface Region2Repository extends JpaRepository<RegionEntity, Long>{

}
