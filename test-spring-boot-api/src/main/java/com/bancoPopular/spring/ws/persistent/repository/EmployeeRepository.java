package com.bancoPopular.spring.ws.persistent.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bancoPopular.spring.ws.persistent.entity.EmployeeEntity;

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Long> {
	
	public List<EmployeeEntity> findByCommissionPctIsNull();
	
	public List<EmployeeEntity> findByCommissionPctIsNotNull();
	
	public List<EmployeeEntity> findByFirstNameStartingWith(String inicio);
	
	public List<EmployeeEntity> findByLastNameEndingWith(String fin);
	
	public List<EmployeeEntity> findByFirstNameContaining(String cadena);
	
	public List<EmployeeEntity> findByFirstNameLike(String like);
	
	public List<EmployeeEntity> findByLastNameLike(String like);
	
	public List<EmployeeEntity> findByFirstNameStartingWithAndLastNameLike(String inicio, String like);
	
	public List<EmployeeEntity> findByFirstNameStartingWithOrLastNameStartingWith(String inicioFirstName, String inicioLastName);
	
	public List<EmployeeEntity> findBySalaryLessThan(Double salary);//<
	public List<EmployeeEntity> findBySalaryLessThanEqual(Double salary); //<=
	
	public List<EmployeeEntity> findBySalaryGreaterThan(Double salary);//>
	
	public List<EmployeeEntity> findBySalaryBetween(Double min, Double max);
	
	public List<EmployeeEntity> findByJobIdIn(List<String> jobIds);
	
	public List<EmployeeEntity> findByHireDateAfter(LocalDate hireDateAfter);
	
	public List<EmployeeEntity> findByHireDateBefore(LocalDate hireDateBefore);
	
	public List<EmployeeEntity> findByOrderBySalary();//Asc
	public List<EmployeeEntity> findByOrderBySalaryAsc();//Asc
	public List<EmployeeEntity> findByOrderBySalaryDesc();//Desc
	
	public List<EmployeeEntity> findByOrderByFirstNameAscLastNameDesc();
	
	public List<EmployeeEntity> findByCommissionPctIsNotNullAndSalaryBetween(Double min, Double max);
	
	public List<EmployeeEntity> findByCommissionPctIsNullAndSalaryGreaterThanOrderBySalaryDesc(Double salaryMin);
	
	////////////////////////////////////////////////
	
	public long countByCommissionPctIsNotNull();
	public Long countByCommissionPctIsNull();
	
	public long countByDepartmentIdInAndHireDateAfter(List<Long> departmentIds, LocalDate hireDateAfter);
	
	////////////////////////////////////////////////
	
	public List<EmployeeEntity> findByCommissionPctIsNotNull(Sort sort);
	
	//public Page<EmployeeEntity> findByCommissionPctIsNotNull(Pageable pageable);
	
	public Slice<EmployeeEntity> findByCommissionPctIsNotNull(Pageable pageable);
	
	///////////////////////////////////////////////
	
	//JPQL -> JPA Query Language
	@Query(name = "buscarPorIdDelJefe", nativeQuery = false, 
			value = "SELECT e "
					+ "FROM EmployeeEntity e "
					+ "WHERE e.managerId = :man_id ")
	
	/*1. Nombres de las tablas -> Nombre de las entidades
	2. Las coolumnas de la tabla -> Atributos de las entidades
	3. Select * -> COnstructor con todos los campos
	4. Select algunas columnas -> Constructor con esas columnas
	5. Reemplazar las variables de reemplazo*/
	public List<EmployeeEntity> buscarPorIdDelJefe(@Param("man_id") Long idJefe);
	
	@Query(nativeQuery = false, 
			value = "SELECT new EmployeeEntity(e.firstName, e.lastName, e.salary, e.commissionPct) "
					+ "FROM EmployeeEntity e "
					+ "WHERE e.managerId = :man_id")
	public List<EmployeeEntity> buscarPorIdDelJefe2(@Param("man_id") Long idJefe);
	
	@Query(name = "buscarPorIdDelJefeSql",
			nativeQuery = true,
			value = "SELECT * "
					+ "FROM employees e "
					+ "WHERE e.manager_id = :man_id ")
	public List<EmployeeEntity> buscarPorIdDelJefeSql(@Param("man_id") Long idJefe);
	
	@Query(nativeQuery = true,
			value = "SELECT e.first_name, e.last_name, e.salary, e.commission_pct "
					+ "FROM employees e "
					+ "WHERE e.manager_id = :man_id")
	public List<Object[]> buscarPorIdDelJefeSql2(@Param("man_id") Long idJefe);
	
	////////////////////////////////////////////////////////
	
	@Modifying
	@Transactional
	@Query(nativeQuery = false,
			value = "UPDATE EmployeeEntity e "
					+ "SET e.salary = :salary "
					+ "WHERE e.id = :id_employee")
	int actualizarSalarioPorID(@Param("id_employee") Long id, @Param("salary") Double salary); 
}
