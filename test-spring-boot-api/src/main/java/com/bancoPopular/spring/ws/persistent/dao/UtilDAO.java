package com.bancoPopular.spring.ws.persistent.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UtilDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void createTable(String tableName) {
		//NO - NUNCA
		/*this.jdbcTemplate.execute("CREATE TABLE " + tableName +  "( "
				+ "	ID BIGINT AUTO_INCREMENT PRIMARY KEY, "
				+ "	NAME VARCHAR(500) NOT NULL	"
				+ ")");*/
		
		String sql = String.format("CREATE TABLE %s ( "
				+ "	ID BIGINT AUTO_INCREMENT PRIMARY KEY, "
				+ "	NAME VARCHAR(500) NOT NULL	"
				+ ")", tableName);
		
		this.jdbcTemplate.execute(sql);
	}
}
