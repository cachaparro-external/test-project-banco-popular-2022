package com.bancoPopular.spring.ws.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TABLE_A")

@Data
public class TableAEntity {

	@Id
	@GeneratedValue(generator = "TableAEntityGenerator", strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	private String name;
	
}
