package com.bancoPopular.spring.ws.persistent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancoPopular.spring.ws.persistent.entity.TableBEntity;

@Repository
public interface TableBRepository extends CrudRepository<TableBEntity, Long> {

}
