package com.bancoPopular.spring.ws.persistent.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="employees")

@Data
//@Getter
//@Setter

@AllArgsConstructor
@NoArgsConstructor

@ToString
public class EmployeeEntity {

	@Id
	@Column(name = "employee_id")
	private Long id;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column
	private String email;
	
	@Column
	private Double salary;
	
	@Column(name = "commission_pct")
	private Double commissionPct;	
	
	@Column(name = "job_id")
	private String jobId;
	
	@Column(name = "hire_date")
	private LocalDate hireDate;
	
	@Column(name = "department_id")
	private Long departmentId;
	
	@Column(name = "manager_id")
	private Long managerId;

	public EmployeeEntity(String firstName, String lastName, Double salary, Double commissionPct) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
		this.commissionPct = commissionPct;
	}
}
