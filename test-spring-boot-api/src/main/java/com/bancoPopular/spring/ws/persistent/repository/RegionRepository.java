package com.bancoPopular.spring.ws.persistent.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;

@Repository
public interface RegionRepository extends CrudRepository<RegionEntity, Long>{

	public Optional<RegionEntity> findByName(String name); 
	
	public List<RegionEntity> findByNameIsNot(String name);
	
	//////////////////////////////////////////////
	
	public long deleteByName(String name);
	public List<RegionEntity> removeByName(String name);
	
	////////////////////////////////////////////
	
	@Modifying
	@Transactional
	@Query(nativeQuery = false,
			value = "DELETE RegionEntity r "
					+ "WHERE r.name = :region_name")
	int borrarPorNombre(@Param("region_name") String regionName); 
}
