package com.bancoPopular.spring.ws.util.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

	@Autowired
	private BasicAuthenticationEntryPoint entryPoint;
	
	@Autowired
	public void addUsers(AuthenticationManagerBuilder builder, PasswordEncoder passwordEncoder) throws Exception {
		builder
			.inMemoryAuthentication()
			.withUser("usuario1")
			.password(passwordEncoder.encode("123456"))
			.roles("CLIENT")
			.and()
			.withUser("usuario2")
			.password(passwordEncoder.encode("987654"))
			.authorities("CONSULTA");
	}
	
	@Bean
	public SecurityFilterChain configureSecurity(HttpSecurity config) throws Exception {
		return config
			//No tienen control de acceso
			.authorizeRequests()
			.antMatchers("/api/banco-popular/dummy")
			.permitAll()
			.and()
			//Control de Acceso
			.authorizeRequests()
			.anyRequest()
			.authenticated()
			.and()
			//Metodo de autenticación
			.httpBasic()
			.authenticationEntryPoint(entryPoint)
			.and()
			.build();
	}
	
	
}
