package com.bancoPopular.spring.ws.persistent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bancoPopular.spring.ws.persistent.entity.EmployeeEntity;

@Repository
public interface Employee2Repository extends JpaRepository<EmployeeEntity, Long>{

}
