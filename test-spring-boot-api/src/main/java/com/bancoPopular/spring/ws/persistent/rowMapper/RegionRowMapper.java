package com.bancoPopular.spring.ws.persistent.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bancoPopular.spring.ws.dto.RegionDTO;

public class RegionRowMapper implements RowMapper<RegionDTO>{

	@Override
	public RegionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		RegionDTO dto = new RegionDTO();
		
		dto.setId(rs.getLong(1));
		dto.setName(rs.getString(2));
		
		return dto;
	}

}
