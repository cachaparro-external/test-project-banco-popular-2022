package com.bancoPopular.spring.ws.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TABLE_B")

@Data
public class TableBEntity {

	@Id
	@GeneratedValue(generator = "TableBEntityGenerator", strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	private String name;
	
	/*@Column(name = "B_ID")
	private Long idB;*/
	
	@OneToOne(fetch = FetchType.EAGER, optional = false, targetEntity = TableAEntity.class)
	@JoinColumn(name = "ID", referencedColumnName = "ID")
	private TableAEntity tableA;
	
}
