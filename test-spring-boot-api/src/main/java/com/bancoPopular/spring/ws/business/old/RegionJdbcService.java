package com.bancoPopular.spring.ws.business.old;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.bancoPopular.spring.ws.dto.FilterDTO;
import com.bancoPopular.spring.ws.dto.RegionDTO;
import com.bancoPopular.spring.ws.persistent.dao.RegionDAO;

@Service
public class RegionJdbcService {

	@Autowired
	private RegionDAO dao;
	
	public List<RegionDTO> getAll(){
		return this.dao.getAll();
	}
	
	public List<RegionDTO> getAllNameStartWith(String start){
		return this.dao.getAllNameStartWith(start);
	}
	
	public List<RegionDTO> getAllIdBetween(Long min, Long max){
		//return this.dao.getAllIdBetween(min, max);
		FilterDTO dto = new FilterDTO();
		dto.setMin(min);
		dto.setMax(max);
		
		return this.dao.getAllIdBetween(dto);
	}
	
	public int countAll(){
		return this.dao.countAll();
	}
	
	public void create(String regionName) {
		Assert.hasText(regionName, "RegionName no puede nulo o vacio");
		
		RegionDTO dto = new RegionDTO();
		dto.setName(regionName);
		
		this.dao.create(dto);
	}
	
}
