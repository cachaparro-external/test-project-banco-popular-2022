package com.bancoPopular.spring.ws.persistent.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bancoPopular.spring.ws.persistent.entity.CountryEntity;

@Repository
public interface CountryRepository extends CrudRepository<CountryEntity, String>{
	
	@Query(nativeQuery = false,
			value = "SELECT c "
					+ "FROM CountryEntity c, RegionEntity r "
					+ "WHERE c.region.id = r.id "
					+ "AND r.name = :region_name "
					+ "ORDER BY c.name ASC")
	public List<CountryEntity> buscarPaisPorNombreRegion(@Param("region_name") String regionName);
	
	@Query(nativeQuery = false,
			value = "SELECT c "
					+ "FROM CountryEntity c "
					+ "INNER JOIN c.region r "
					+ "WHERE r.name = :region_name "
					+ "ORDER BY c.name ASC")
	public List<CountryEntity> buscarPaisPorNombreRegionMejorada(@Param("region_name") String regionName);
	
	@Query(nativeQuery = true,
			value = "SELECT c.country_id , c.country_name , r.region_id , r.region_name  "
					+ "FROM countries c, regions r "
					+ "WHERE c.region_id = r.region_id "
					+ "AND r.region_name = :region_name "
					+ "ORDER BY c.country_name ASC ")
	public List<Object[]> buscarPaisPorNombreRegion2(@Param("region_name") String regionName);
	
	public List<CountryEntity> findByNameOrderByNameAsc(String name);
}
