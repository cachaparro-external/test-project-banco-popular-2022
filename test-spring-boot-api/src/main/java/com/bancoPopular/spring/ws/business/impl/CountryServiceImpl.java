package com.bancoPopular.spring.ws.business.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.bancoPopular.spring.ws.business.CountryService;
import com.bancoPopular.spring.ws.dto.CountryDTO;
import com.bancoPopular.spring.ws.persistent.entity.CountryEntity;
import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;
import com.bancoPopular.spring.ws.persistent.repository.CountryRepository;
import com.bancoPopular.spring.ws.persistent.repository.RegionRepository;
import com.bancoPopular.spring.ws.util.exception.EntityAlreadyExistsException;
import com.bancoPopular.spring.ws.util.exception.EntityNotFoundException;

@Service
public class CountryServiceImpl implements CountryService{
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private RegionRepository regionRepository;
	
	/*@Autowired
	private HRMapper mapper;*/
	
	@Autowired
	private DozerBeanMapper mapper;

	@Override
	public List<CountryDTO> getByName(String name) {
		List<CountryDTO> countries = new ArrayList<>();
		List<CountryEntity> countryList = this.countryRepository.findByNameOrderByNameAsc(name);
		
		countryList.forEach(country -> {
			CountryDTO dto = this.mapper.map(country, CountryDTO.class);
			//MapStruct -> this.mapper.convertFromCountryEntityToCountryDTO(country);
			//dto.setId(country.getId());
			//dto.setName(country.getName());
			//dto.setRegionName(country.getRegion().getName());
			
			countries.add(dto);
		});
		
		return countries;
	}

	@Override
	public String create(CountryDTO dto) {
		if(!this.countryRepository.existsById(dto.getId())) {
			Optional<RegionEntity> regionOpt = this.regionRepository.findByName(dto.getRegionName());
			
			if(regionOpt.isPresent()) {
				CountryEntity entity = this.mapper.map(dto, CountryEntity.class);
				//this.mapper.convertFromCountryDTOToCountryEntity(dto);
				//entity.setId(dto.getId());
				//entity.setName(dto.getName());
				
				RegionEntity regionEntity = new RegionEntity();
				regionEntity.setId(regionOpt.get().getId());
				
				entity.setRegion(regionEntity);
				
				entity = this.countryRepository.save(entity);
				
				return entity.getId();
			}else {
				//return null;
				
				//Excepción personalizada RegionNotFound
				throw new IllegalArgumentException(String.format("Región %s no existe", dto.getRegionName()));
			}
		}else {
			throw new EntityAlreadyExistsException(String.format("Country %s ya existe", dto.getId()));
		}	
	}

	@Override
	public void modify(CountryDTO dto) {
		Optional<CountryEntity> entityOpt = this.countryRepository.findById(dto.getId());
		
		if(entityOpt.isPresent()) {
			entityOpt.get().setName(dto.getName());
			
			this.countryRepository.save(entityOpt.get());
		}else {
			throw new EntityNotFoundException(String.format("Country %s no existe", dto.getId()));
		}
	}

	@Override
	public List<CountryDTO> getAll() {
		List<CountryDTO> countries = new ArrayList<>();
		Iterable<CountryEntity> countryList = this.countryRepository.findAll();
		
		countryList.forEach(country -> {
			CountryDTO dto = this.mapper.map(country, CountryDTO.class);
			
			countries.add(dto);
		});
		
		return countries;
	}

	@Override
	public void deleteById(String id) {
		try {
			this.countryRepository.deleteById(id);
		}catch(DataIntegrityViolationException e){
			throw new EntityNotFoundException(String.format("Country con ID %s no existe", id));
		}
	}

}
