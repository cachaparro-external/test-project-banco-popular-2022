package com.bancoPopular.spring.ws.business.old;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.bancoPopular.spring.ws.persistent.entity.EmployeeEntity;
import com.bancoPopular.spring.ws.persistent.repository.Employee2Repository;
import com.bancoPopular.spring.ws.persistent.repository.Employee3Repository;
import com.bancoPopular.spring.ws.persistent.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository empRep;
	
	@Autowired
	private Employee2Repository emp2Rep;
	
	@Autowired
	private Employee3Repository emp3Rep;
	
	public void actualizar(Long idEmployee, Double newSalary) {
		//Forma 1: Consulta previa
		/*Optional<EmployeeEntity> employeeOpt = this.empRep.findById(idEmployee);
		
		if(employeeOpt.isPresent()) {
			EmployeeEntity employee = employeeOpt.get();
			
			employee.setSalary(newSalary);
			
			this.empRep.save(employee);
		}else {
			return;
		}*/
		
		//Forma 2: Usando solo los campos que quiero actualizar
		//No es viable a menos que actualice todos los campos
		/*EmployeeEntity employee = new EmployeeEntity();
		employee.setId(idEmployee);
		employee.setSalary(newSalary);
		
		this.empRep.save(employee);*/
		
		//Forma 3: Usando @Query
		int cantRegActualizados = this.empRep.actualizarSalarioPorID(idEmployee, newSalary);
		
		System.out.println("Cantidad de registros actualizados: " + cantRegActualizados);
	}
	
	public Page<EmployeeEntity> getAllPageable(int page, int size) {
		//return this.emp3Rep.findAll(PageRequest.of(page, size));
		return this.emp3Rep.findAll(PageRequest.of(page, size, Sort.by(Order.asc("salary"))));
	}
	
	public Page<EmployeeEntity> getAllByDepartmentPageable(Long departmentId, int page, int size) {
		return this.emp3Rep.findByDepartmentId(departmentId, PageRequest.of(page, size, Sort.by(Order.asc("id"))));
	}
	
	public List<EmployeeEntity> getBySalaryAndJobId(Double salary, List<String> jobIds){
		//return this.emp3Rep.findBySalaryLessThanAndCommissionPctIsNullAndJobIdIn(salary, jobIds, Sort.by(Order.asc("firstName"), Order.asc("lastName")));
		//return this.emp3Rep.findBySalaryLessThanAndCommissionPctIsNullAndJobIdIn(salary, jobIds, Sort.by(Direction.ASC, "firstName", "lastName"));
		return this.emp3Rep.findBySalaryLessThanAndCommissionPctIsNullAndJobIdIn(salary, jobIds, Sort.by("firstName").ascending().and(Sort.by("lastName").ascending()));
	}
	
}
