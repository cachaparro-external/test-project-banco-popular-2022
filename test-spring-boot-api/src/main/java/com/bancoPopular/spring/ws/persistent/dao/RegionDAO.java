package com.bancoPopular.spring.ws.persistent.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bancoPopular.spring.ws.dto.FilterDTO;
import com.bancoPopular.spring.ws.dto.RegionDTO;
import com.bancoPopular.spring.ws.persistent.rowMapper.RegionRowMapper;

@Repository
public class RegionDAO {
	
	//SELECT sin parametros, DDL
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	//SELECT, DML con parametros de reemplazo
	@Autowired
	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	public List<RegionDTO> getAll() {
		List<RegionDTO> regions = this.jdbcTemplate.query("SELECT r.region_id, r.region_name FROM regions r ORDER BY r.region_name ASC", 
				new RegionRowMapper());
		
		return regions;
	}
	
	public List<RegionDTO> getAllNameStartWith(String start) {
		Map<String, Object> params = new HashMap<>();
		params.put("region_name_start", start);
		
		List<RegionDTO> regions = this.namedParamJdbcTemplate.query(
				"SELECT r.region_id, r.region_name FROM regions r WHERE SUBSTR(r.region_name, 1, 1) = :region_name_start ORDER BY r.region_name DESC",
				params,
				new RowMapper<RegionDTO>() {

					@Override
					public RegionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
						RegionDTO dto = new RegionDTO();
						
						dto.setId(rs.getLong("region_id"));
						dto.setName(rs.getString("region_name"));
						
						return dto;
					}
				});
		
		return regions;
	}
	
	public List<RegionDTO> getAllIdBetween(Long min, Long max) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("min", min);
		params.addValue("max", max);
		
		List<RegionDTO> regions = this.namedParamJdbcTemplate.query(
				"SELECT r.region_id, r.region_name FROM regions r WHERE r.region_id BETWEEN :min AND :max ORDER BY r.region_id ASC",
				params,
				(rs, rowNum) -> {
					RegionDTO dto = new RegionDTO();
					
					dto.setId(rs.getLong("region_id"));
					dto.setName(rs.getString("region_name"));
					
					return dto;
				});
		
		return regions;
	}
	
	public List<RegionDTO> getAllIdBetween(FilterDTO filterDto) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(filterDto);
		
		List<RegionDTO> regions = this.namedParamJdbcTemplate.query(
				"SELECT r.region_id AS id, r.region_name AS name FROM regions r WHERE r.region_id BETWEEN :min AND :max ORDER BY r.region_id ASC",
				params,
				new BeanPropertyRowMapper<>(RegionDTO.class));
		
		return regions;
	}
	
	public void create(RegionDTO dto) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("region_name", dto.getName());
		
		int cantRegInsertados = this.namedParamJdbcTemplate.update(
				"INSERT INTO regions (region_name) "
				+ "VALUES (:region_name)", 
				params);
		
		System.out.println("Se insertaron " + cantRegInsertados + " registros");	
	}
	
	public void callProcedure() {
		List<SqlParameter> params = new ArrayList<>();
		
		params.add(new SqlParameter(Types.VARCHAR));
		params.add(new SqlParameter(Types.NUMERIC));
		
		this.jdbcTemplate.call(
				new CallableStatementCreator() {
					
					@Override
					public CallableStatement createCallableStatement(Connection con) throws SQLException {
						// TODO Auto-generated method stub
						return null;
					}
				}, params);
	}
	
	public int countAll() {
		return this.jdbcTemplate.queryForObject("SELECT COUNT(r.region_id) FROM regions r", Integer.class);
	}
}
