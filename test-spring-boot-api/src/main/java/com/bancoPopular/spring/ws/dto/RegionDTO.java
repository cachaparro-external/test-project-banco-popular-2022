package com.bancoPopular.spring.ws.dto;

import lombok.Data;

@Data
public class RegionDTO {
	
	private Long id;
	private String name;

}
