package com.bancoPopular.spring.ws.business;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bancoPopular.spring.ws.dto.CountryDTO;

@Service
public interface CountryService {

	public List<CountryDTO> getByName(String name);
	
	public String create(CountryDTO dto);
	
	public void modify(CountryDTO dto);
	
	public List<CountryDTO> getAll();
	
	public void deleteById(String id);
	
}
