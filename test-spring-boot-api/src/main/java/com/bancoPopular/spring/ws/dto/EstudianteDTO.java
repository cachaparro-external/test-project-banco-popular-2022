package com.bancoPopular.spring.ws.dto;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "DTO que contiene la información de los estudiantes")
public class EstudianteDTO {

	@ApiModelProperty(value = "ID del estudiante", required = true)
	private Long id;
	
	@NotBlank
	@Size(min = 3)
	@ApiModelProperty(value = "Nombre del estudiante", required = true)
	private String firstName;
	@NotBlank
	@Size(min = 3)
	@ApiModelProperty(value = "Apellido del estudiante", required = true)
	private String lastName;
	@NotNull
	@Past()
	@ApiModelProperty(value = "Fecha de nacimiento del estudiante", required = true, example = "DD/MM/YYYY")
	private LocalDate birthDate;
	
	@Email
	//@Pattern(regexp = "?[a-zA-Z0-9]{*}$")
	@ApiModelProperty(value = "correo del estudiante", required = true, example = "pepito@test.com")
	private String email;
	
	@ApiModelProperty(value = "telefono del estudiante", required = true, example = "3001234567")
	private String phoneNumber;
	
}
