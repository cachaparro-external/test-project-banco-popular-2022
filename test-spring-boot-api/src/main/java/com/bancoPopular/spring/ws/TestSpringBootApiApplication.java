package com.bancoPopular.spring.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.bancoPopular.spring.ws.business.CountryService;
import com.bancoPopular.spring.ws.dto.CountryDTO;

import lombok.extern.log4j.Log4j2;

@SpringBootApplication

@Log4j2
public class TestSpringBootApiApplication {
	
	//private static final Log log = LogFactory.getLog(TestSpringBootApiApplication.class);

	/*@Autowired
	private RegionRepository regionRep;*/
	
	/*@Autowired
	private Employee2Repository empRep;*/
	
	/*@Autowired
	private CountryRepository countryRep;*/
	
	/*@Autowired
	private CountryOldService countrySer;
	
	@Autowired
	private RegionService regionSer;
	
	@Autowired
	private TableBService tableBSer;
	
	@Autowired
	private TransactionExample transactionExample;
	
	@Autowired
	private EmployeeService emplSer;
	
	@Autowired
	private RegionJdbcService regionJdbcSer;
	
	@Autowired
	private UtilService utilSer;*/
	
	@Autowired
	private CountryService countryService;
	
	/*@Autowired
	private DozerBeanMapper mapper;*/
	
	/*@Autowired
	private TestMapper mapper;*/
	
	public static void main(String[] args) {
		SpringApplication.run(TestSpringBootApiApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			/*long cantidadRegiones = this.regionRep.count(); //Contar la cantidad de registros

			System.out.println("La cantidad de regiones es: " + cantidadRegiones);*/
			
			//Delete
			//this.regionRep.delete(null);
			//this.regionRep.deleteAll();
			//this.regionRep.deleteAllById(null);//Borra una lista de ID's
			
			/*if(this.regionRep.existsById(5L)) {
				this.regionRep.deleteById(5L);
				
				System.out.println("La región 5 fue borrada");
			}else {
				System.out.println("La región con ID 5 no existe");
			} */
			
		 	/*Iterable<RegionEntity> regiones = this.regionRep.findAll();
		 	
		 	regiones.forEach((region) -> {
		 		System.out.println("La región " + region.getId() + ": " + region.getName());
		 	});*/
		 	
		 	//this.regionRep.findAllById(null)
		 	/*Optional<RegionEntity> regionOpt1 = this.regionRep.findById(1L);
		 	
		 	if(regionOpt1.isPresent()) {
		 		RegionEntity region = regionOpt1.get();
		 		
		 		System.out.println("La región " + region.getId() + ": " + regionOpt1.get().getName());
		 	}else {
		 		System.out.println("La región con ID 1 no existe");
		 	}
		 	
		 	Optional<RegionEntity> regionOpt2 = this.regionRep.findById(99999L);
		 	
		 	if(regionOpt2.isEmpty()) {
		 		System.out.println("La región con ID 99999 no existe");		 		
		 	}else {
		 		System.out.println("La región con ID 99999 existe");	
		 	}*/
			
			//this.regionRep.saveAll(null)
			
			//1. Paso el ID nulo -> CREA
			//2. Paso el ID no nulo, pero no existe en BD -> CREA
			//3. Paso el ID no nulo, existe en BD ->
			/*RegionEntity region = new RegionEntity();
			region.setId(6L);
			region.setName("Africa");
			
			region = this.regionRep.save(region);
			//this.regionRep.save(region);
			
			System.out.println("La región " + region.getName() + " se creo/modifico con ID " + region.getId());*/
			
			//Taller 12/10/2022
			//Entity    Countries -> CountryEntity
			//Contar todos los registros
			//Borrar por ID - Verificar si existe
			//Consultar todos los registros
			//Consultar un registro por ID
			//Crear un nuevo registro
			
			//Optional<RegionEntity> regionOpt1 = this.regionRep.findByName("Asia");
			/*Optional<RegionEntity> regionOpt1 = this.regionRep.findByName("Asia1");
			
			if(regionOpt1.isPresent()) {
				System.out.println("La región con nombre " + regionOpt1.get().getName() + " tiene ID: " + regionOpt1.get().getId());
			}else {
				System.out.println("La región no existe");
			}*/
			
			/*List<RegionEntity> regiones = this.regionRep.findByNameIsNot("Asia");
			
			regiones.forEach((region) -> {
				System.out.println("Región " + region.getId() + ": " + region.getName());
			});*/
			
			//EmployeeEntity employee = new EmployeeEntity();
			
			//List<EmployeeEntity> empleados = this.empRep.findByCommissionPctIsNull();
			//List<EmployeeEntity> empleados = this.empRep.findByCommissionPctIsNotNull();
			//List<EmployeeEntity> empleados = this.empRep.findByFirstNameStartingWith("Pe");
			//List<EmployeeEntity> empleados = this.empRep.findByLastNameEndingWith("s");
			//List<EmployeeEntity> empleados = this.empRep.findByFirstNameContaining("ne");
			//List<EmployeeEntity> empleados = this.empRep.findByLastNameLike("S%");
			//List<EmployeeEntity> empleados = this.empRep.findByFirstNameStartingWithAndLastNameLike("S", "S%");
			//List<EmployeeEntity> empleados = this.empRep.findByFirstNameStartingWithOrLastNameStartingWith("S", "S");
			
			//List<EmployeeEntity> empleados = this.empRep.findBySalaryLessThan(7000.0);
			//List<EmployeeEntity> empleados = this.empRep.findBySalaryGreaterThan(5000.0);
			//cfindBySalaryBetween(3000.0, 6000.0);
			
			/*List<String> jobIds = new ArrayList<>();
			jobIds.add("SA_REP");
			jobIds.add("SH_CLERK");
			jobIds.add("AD_PRES");*/
			
			//List<EmployeeEntity> empleados = this.empRep.findByJobIdIn(jobIds);
			
			//List<EmployeeEntity> empleados = this.empRep.findByJobIdIn(Arrays.asList("SA_REP", "SH_CLERK", "AD_PRES"));
			
			//List<EmployeeEntity> empleados = this.empRep.findByHireDateBefore(LocalDate.of(1998, 1, 1));
			//List<EmployeeEntity> empleados = this.empRep.findByHireDateAfter(LocalDate.of(1998, 1, 1));
			
			//List<EmployeeEntity> empleados = this.empRep.findByOrderBySalaryAsc();
			/*List<EmployeeEntity> empleados = this.empRep.findByOrderBySalaryDesc();
			
			empleados.forEach((empleado) -> {
				System.out.println("Empleado: " + empleado.toString());
			});*/
			
			//13/10/2022
			//Consultar todos los empleados cuyo nombre (first name) tiene 3 letras
			//Consultar todos los empleados cuyo nombre y apellido (last_name) terminan con a
			//Consultar todos los empleados cuyo nombre empieza con M y cuyo apellido termina con s
			
			//13/10/2022
			//Consultar todos los empleados que pertenecen al departameno 50, 60, 90 y tienen un salario superior a 5000
			//Consultar todos los empleados que ingresaron despues del 01-01-1999 y el cargo es SA_REP o PU_CLERK
			//Consultar todos los empleados que tienen comisión y devengan un salario entre 5000 y 10.000
			
			//Order BY con Filtro
			//Conteos
			//Delete
			//@TRansactional
			//@Modifying con delete y update
			//JpaRepository
			//PagingAndSortingRepository
			
			//List<EmployeeEntity> empleados = this.empRep.findByCommissionPctIsNullAndSalaryGreaterThanOrderBySalaryDesc(8000.0);
			//List<EmployeeEntity> empleados = this.empRep.findByOrderByFirstNameAscLastNameDesc();
			
			/*List<EmployeeEntity> empleados = this.empRep.findByCommissionPctIsNotNull(Sort.by(Order.asc("firstName")));
			
			List<EmployeeEntity> empleados1 = this.empRep.findByCommissionPctIsNotNull(
					Sort.by(Order.asc("firstName"), Order.desc("lastName")));
			
			List<EmployeeEntity> empleados2 = this.empRep.findByCommissionPctIsNotNull(
							Sort.by(Direction.DESC, "firstName", "lastName"));
			
			List<EmployeeEntity> empleados3 = this.empRep.findByCommissionPctIsNotNull(
					Sort.by("salary").ascending().and(Sort.by("firtsName").descending()));*/
			
			//Primera pagina
			//Page<EmployeeEntity> empleados1 = this.empRep.findByCommissionPctIsNotNull(Pageable.ofSize(10));
			
			//Sin paginación
			//Page<EmployeeEntity> empleados2 = this.empRep.findByCommissionPctIsNotNull(Pageable.unpaged());
			
			//Pagina 1
			//Page<EmployeeEntity> empleados = this.empRep.findByCommissionPctIsNotNull(PageRequest.of(0, 10));
			
			/*System.out.println("Slice number " + empleados.getNumber());
			System.out.println("Slice number of elements " + empleados.getNumberOfElements());
			System.out.println("Slice size of slice " + empleados.getSize());
			System.out.println("Get total rows: " + empleados.getTotalElements());
			System.out.println("Get total pages: " + empleados.getTotalPages());
			System.out.println("Has content " + empleados.hasContent());
			System.out.println("Has next " + empleados.hasNext());
			System.out.println("Has previous " + empleados.hasPrevious());
			System.out.println("Is empty " + empleados.isEmpty());
			System.out.println("Is first " + empleados.isFirst());
			System.out.println("Is last " + empleados.isLast());*/
			
			//Ordenados
			//Page<EmployeeEntity> empleados1 = this.empRep.findByCommissionPctIsNotNull(PageRequest.of(0, 10, Sort.by(Order.asc("salary"))));
			
			//Page<EmployeeEntity> empleados = this.empRep.findByCommissionPctIsNotNull(PageRequest.of(0, 10, Direction.ASC, "salary"));
			
			/*empleados.forEach((empleado) -> {
				System.out.println("Empleado: " + empleado.toString());
			});*/
			
			/*int page = 0;
			final int SIZE = 10;
			
			Page<EmployeeEntity> empleados;
			
			do {
				empleados = this.empRep.findByCommissionPctIsNotNull(PageRequest.of(page, SIZE));
				
				System.out.println("Pagina " + page);
				
				empleados.forEach((empleado) -> {
					System.out.println("Empleado: " + empleado.toString());
				});
				
				page ++;
			}while(empleados.hasNext());
			
			Page<EmployeeEntity> empleados1;
			Pageable pageable = Pageable.ofSize(SIZE);
			
			do {
				empleados1 = this.empRep.findByCommissionPctIsNotNull(pageable);
				
				System.out.println("Pagina " + empleados1.getNumber());
				
				empleados1.forEach((empleado) -> {
					System.out.println("Empleado: " + empleado.toString());
				});
				
				pageable = empleados1.nextPageable();
			}while(empleados1.hasNext());*/
			
			/*final int SIZE = 10;
			
			Slice<EmployeeEntity> empleados1;
			Pageable pageable = Pageable.ofSize(SIZE);
			
			do {
				empleados1 = this.empRep.findByCommissionPctIsNotNull(pageable);
				
				System.out.println("Pagina " + empleados1.getNumber());
				
				empleados1.forEach((empleado) -> {
					System.out.println("Empleado: " + empleado.toString());
				});
				
				pageable = empleados1.nextPageable();
			}while(empleados1.hasNext());*/
			
			//List<EmployeeEntity> empleados = this.empRep.buscarPorIdDelJefe(108L);
			//List<EmployeeEntity> empleados = this.empRep.buscarPorIdDelJefe2(108L);
			
			//List<EmployeeEntity> empleados = this.empRep.buscarPorIdDelJefeSql(108L);
						
			/*empleados.forEach((empleado) -> {
				System.out.println("Empleado: " + empleado.toString());
			});*/
			
			/*List<Object[]> empleados = this.empRep.buscarPorIdDelJefeSql2(108L);
			
			final int FIRST_NAME = 0;
			final int LAST_NAME = 1;
			final int SALARY = 2;
			
			empleados.forEach((empleado) -> {
				System.out.println("Empleado: " + empleado[FIRST_NAME] + " " + empleado[LAST_NAME] + " - " + empleado[SALARY]);
			});*/
			
			//long cantidadRegistros = this.empRep.countByCommissionPctIsNotNull();
			//long cantidadRegistros = this.empRep.countByCommissionPctIsNull();
			//long cantidadRegistros = this.empRep.countByDepartmentIdInAndHireDateAfter(Arrays.asList(50L, 60L), LocalDate.of(1998, 1, 1));
			
			//System.out.println("La cantidad de registro es: " + cantidadRegistros);
			
			//long cantRegistrosBorrados = this.regionRep.deleteByName("Oceania");
			//long cantRegistrosBorrados = this.transactionExample.deleteRegionByName("Oceania");
			//System.out.println("La cantidad de registros borrados es: " + cantRegistrosBorrados);
			
			
			/*try {
				this.transactionExample.executeTransaction();
			}catch (TransactionException e) {
				log.error("Error en transacción", e);
				
				//log.error("Se presento una excepción", e);
			}*/
			
			/*Atomicy
			Consistency
			Isolation
			Durability
			
			Insert
			update
			delete
			Commit;*/
			
			//18/10/2022
			/*
			 * CountryEntity
			 * 
			 * Actualizar el nombre de un País
			 * Crear un nuevo país
			 * Borrar un pais
			 * 
			 * Commit exitoso
			 * 
			 * Actualizar el nombre de un País
			 * Crear un nuevo pais a una región que no existe 999
			 * Borrar un país
			 * 
			 * Commit fallido
			 * 
			 */
			
			//19/10/2022
			//1. Consultar todos los empleados que tienen un salario mayor a X y tienen comision (JPQL y SQL) -> Todas las columnas
			//2. Consultar todos los emplados cuya fecha de ingreso es superior a X y pertenecen a los departamentos 50, 60 y 70 -> (JPQL y SQL) -> firstName, LastName, Salary, HireDate y Department ID
			
			/*Optional<CountryEntity> countryOpt = this.countryRep.findById("AR");
			
			if(countryOpt.isPresent()) {
				System.out.println("Country: " + countryOpt.get());
				
				Optional<RegionEntity> regionOpt = this.regionRep.findById(countryOpt.get().getRegionId());
				
				System.out.println("Region: " + regionOpt.get());
			}else {
				System.out.println("No existe el país");
			}*/
			
			//CountryEntity country = this.countrySer.getById("AR");
			/*boolean includeRegion = false;
			CountryEntity country = this.countrySer.getById("AR", includeRegion);
			
			if(country != null) {
				System.out.println("Country ID: " + country.getId());
				System.out.println("Country Name: " + country.getName());
				
				if(includeRegion) {
					System.out.println("Country - Region Name: " + country.getRegion().getName());
				}
			}else {
				System.out.println("No existe el país");
			}*/
			
			/*RegionEntity region = this.regionSer.getById(1L);
			
			if(region != null) {
				System.out.println("Region: " + region);
			}else {
				System.out.println("No existe la región");
			}*/
			
			/*TableBEntity tableB = this.tableBSer.getById(1L);
			
			if(tableB != null) {
				System.out.println("Table B: " + tableB);
			}else {
				System.out.println("No existe el registro");
			}*/
			
			//20/10/2022
			//1. Crear la entidad para LOcations, Department y Jobs
			//2. Enlazar Location con Country
			//3. Enlazar Depatment con Location
			//4. Enlzar Empleados con Deparmetn y Job
			
			/*List<CountryEntity> countries = this.countrySer.getByRegionName("Europe");
			countries.forEach((country) -> {
				System.out.println("País: " + country.getId() + " - " + country.getName() + " - " + country.getRegion().getName());
			});*/
			
			//List<Object[]> countries = this.countrySer.getByRegionName2("Europe");
			
			/*countries.forEach((country) -> {
				System.out.println("País: " + Arrays.asList(country));
			});*/
			
			//24/10/2022
			//1. Crear un Query JPQL que involucre region, country, location, department, employee y job, donde el salario se superior a un monto y tenga comisión --> Opciṕn 2 (JPA infiere el join)
			//2. Crear un Query SQL que involucre region, country, location, department, employee y job, donde el salario se superior a un monto y tenga comisión
			
			//this.emplSer.actualizar(109L, 9500.0);
			//this.regionSer.borrarPorNombre("Africa Norte");
			
			//Long idRegion = this.regionSer.create("Oceania Sur");
			//Long idRegion = this.regionSer.createAndFlush("Africa Sur");
			
			//System.out.println("El ID es: " + idRegion);
			
			//final int SIZE = 10;
			
			//Page<EmployeeEntity> page = this.emplSer.getAllPageable(5, SIZE);
			//Page<EmployeeEntity> page = this.emplSer.getAllByDepartmentPageable(50L, 0, SIZE);
			
			/*page.forEach((employee) -> {
				System.out.println("Empleado: " + employee);
			});
			
			System.out.println("La cantidad de registros es: " + page.getTotalElements());
			System.out.println("La cantidad de paginas es: " + page.getTotalPages());*/
			
			//25/10/2022
			//Implementar la logica para consultar todos los empleados paginados, me debe mostrar la pagina
			//Pagina 1:
			//Empl 1
			//Empl 2
			//...
			//Pagina 2:
			//Empl 20
			//Empl 21
			//...
			
			//1. Implementar un proceso que haga la creación de un Pais (que no exista), la actualización del nombre de una pais y el borrado de un pais, usando JpaRepository
			//Se debe hacer flush en cada ejecución, Recuerde que debe ser transaccional
			//2. Crear una consulta derivada que permita (paginada y usando Sort):
			//2.1 Consultar los empleados cuya fecha de ingreso sea superior a un valor y pertenezca a un conjunto de departamentos (por ejemplo: 50, 60, 70), Ordenada por ID de forma DESC
			//2.2 Consultar los empleados cuyo salario es inferior a un valor, no tienen comisión y pertenecen a un conjunto de cargos (por ejemplo: SA_REP, SA_CLERK), ordenada por nombre y apellido de forma ASC
			//2.3 Consultar los empleados cuyo jefe es el mismo, ordenada por ordenada por nombre y apellido de forma DESC
			//3. Crear una consulta JPQL que permita (paginada y usando Sort):
			//3.1 Consultar los empleados cuyo nombre empieza por vocal ordenada por nombre ASC
			//3.2 Consultar los empleados cuyo nombre y apellido empiezan por la misma letra, ordenada por nombre ASC y apellido DESC
			/*SELECT *
			FROM employees e 
			where SUBSTR(e.first_name, 1, 1) = SUBSTR(e.last_name, 1, 1);*/
			
			//List<RegionDTO> regions = this.regionJdbcSer.getAll();
			//List<RegionDTO> regions = this.regionJdbcSer.getAllNameStartWith("A");
			/*List<RegionDTO> regions = this.regionJdbcSer.getAllIdBetween(1L, 5L);
			
			regions.forEach((region) -> {
				System.out.println("Region: " + region);
			});*/
			
			//System.out.println("Cantidad de regiones: " + this.regionJdbcSer.countAll());  
			
			//26/10/2022 -> JDBC Template
			//1. Consulta de todos los paises (CountryDTO), ordenado por ID de forma ASC
			//2. Consultar los paises por region (ID), ordenado por nombre de pais ASC
			//3. Consultar los paises cuyo nombre terminan en una letra.
			
			//27/10/2022
			//1. Consultar los paises por region (ID), ordenado por nombre de pais ASC (BeanPropertySqlParameterSource y BeanPropertyRowMapper)
			
			//this.utilSer.createTable("TEST_B");
			
			//this.regionJdbcSer.create("Region de prueba 1");
			
			//2. Proveer una capacidad que permita crear (ID, Nombre, Region), actualizar (ID, Nombre) y borrar paises (ID).
			
			/*List<CountryDTO> countries = this.countryService.getByName("Argentina");
			
			countries.forEach(country -> {
				System.out.println("Country: " + country);
			});*/
			
			/*try {
				CountryDTO dto = new CountryDTO("AR", "Argentina 3", "Americas 1");				
				
				this.countryService.modify(dto);
				
				//String idCountry = this.countryService.create(dto);
				//System.out.println("Se creo pais con ID " + idCountry);
			}catch (IllegalArgumentException e) {
				System.err.println(e.getMessage());
			}*/
			
			//03/11/2022
			//Empleado Servicio (Interface + Impl) --> EmployeeDTO
			//1. Crear metodo que permita consulta un empleado por ID, si no existe debe lanzar una excepción -> El retorno es un EmployeeDTO
			//2. Crear metodo que permita consultar todos los empleados que pertenecen a un departamento (nombre) y los retorne, en caso de que el depto no exista, debe lanzar excepción 
			//  --> Lista, si no hay registros, no debe mostrar, retorna la lista vacia
			//3. Crear metodo que permita crear un empleado, first_name, last_name, hire_date, job_id (nombre del cargo), salario, nombre del departamento, retornar el ID del empleado
			//4. Crear metodo que permita actualizar el salario y la comision de un empleado por ID, no importa si el empleado no existe  -> Retorno es void
			
			//TestMapper mapper = Mappers.getMapper(TestMapper.class);
			
			/*TestADTO a = new TestADTO();
			a.setId(1L);
			a.setName("Jose"); 
			
			A1 a1 = new A1();
			a1.setName("Rodrigo Velez");
			a1.setCountry("Colombia");
			
			a.setA1(a1);
			
			System.out.println("Objeto A: " + a);
			
			TestBDTO b = mapper.testADtoToTestBDto(a);
			
			System.out.println("Objeto B: " + b);
			
			b.getB1().setFirstName("Alberto");
			b.getB1().setLastName("Poveda");
			
			TestADTO testA = mapper.testBDtoToTestADto(b);
			
			System.out.println(testA);*/
			
			//08/11/2022
			//Usando el metodo de consulta de empleado y empleados, crear el Mapper que permite obtener de EmployeeEntity --> EmployeeDTO y viceversa, 
			//Inicialmente empleado, luego van agregando Departamento, Job, location ...
			
			//DozerBeanMapper mapper = new DozerBeanMapper();
			
			/*TestADTO a = new TestADTO();
			a.setId(1L);
			a.setName("Jose"); 
			
			A1 a1 = new A1();
			a1.setName("Rodrigo Velez");
			a1.setCountry("Colombia");
			
			a.setA1(a1);
			
			System.out.println("Objeto A: " + a);
			
			TestBDTO b = mapper.map(a, TestBDTO.class);
			System.out.println("Objeto B: " + b); */
			
			//09/11/2022
			 //Crear el metodo de consulta de departamento por ID (1) y departamento por ID de jefe (Lista), 
			//crear el Mapper que permite obtener de DeptoEntity --> DeptoDTO y viceversa, Utilizando Dozer			
		};
	} 
}
