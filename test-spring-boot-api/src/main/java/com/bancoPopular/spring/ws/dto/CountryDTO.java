package com.bancoPopular.spring.ws.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryDTO {

	@NotBlank(message = "ID es nulo")
	@Size(min = 2, max = 2, message = "El ID debe tener 2 caracteres")
	private String id;
	@NotBlank(message = "Name es nulo")
	@Size(min = 3, message = "El Name debe tener minimo 3 caracteres")
	private String name;
	@NotBlank(message = "RegionName es nulo")
	@Size(min = 3, message = "El RegionName debe tener minimo 3 caracteres")
	private String regionName;
	
}
