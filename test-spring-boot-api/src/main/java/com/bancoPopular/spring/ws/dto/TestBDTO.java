package com.bancoPopular.spring.ws.dto;

import org.dozer.Mapping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestBDTO {

	//private Long id;
	//private String name;
	//@Mapping("id")
	private Long idTest;
	//@Mapping("name")
	private String nameTest;
	
	private B1 b1;
	
}
