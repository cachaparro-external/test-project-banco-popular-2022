package com.bancoPopular.spring.ws.api;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bancoPopular.spring.ws.business.CountryService;
import com.bancoPopular.spring.ws.dto.CountryDTO;
import com.bancoPopular.spring.ws.util.exception.EntityAlreadyExistsException;
import com.bancoPopular.spring.ws.util.exception.EntityNotFoundException;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/api/banco-popular/country")
@Log4j2
public class CountryAPI {
	
	@Autowired
	private CountryService service;
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<CountryDTO>> getAll() {
		try {
			return new ResponseEntity<List<CountryDTO>>(this.service.getAll(), HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en getAll", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/name/{name}")
	public ResponseEntity<List<CountryDTO>> getByName(
			@PathVariable(value = "name") String name
			) {
		try {
			return new ResponseEntity<List<CountryDTO>>(this.service.getByName(name), HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en getByName", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/filter")
	public ResponseEntity<List<CountryDTO>> getByName2(
			@RequestParam(value = "name", required = true) String name
			) {
		try {
			return new ResponseEntity<List<CountryDTO>>(this.service.getByName(name), HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en getByName", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/*@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String> create(
			@RequestBody CountryDTO dto
			){
		try {
			//if(dto.getId() != null && !dto.getId().equals("") && !dto.getId().trim().equals("")) {
			if(this.validateCreateRequest(dto)) {
			//if(!dto.getId().isBlank()) {
				String countryId = this.service.create(dto);
				
				return new ResponseEntity<>(countryId, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}		
			
			
			/*if(StringUtils.isNotBlank(dto.getId())) {
				if(StringUtils.isNotBlank(dto.getName())) {
					if() {
						
					}else {
						
					}
				}else {
					return new ResponseEntity<>("Name es nulo", HttpStatus.BAD_REQUEST);
				}
				
			}else {
				return new ResponseEntity<>("ID es nulo", HttpStatus.BAD_REQUEST);
			}*/
		/*}catch (IllegalArgumentException e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}catch (EntityAlreadyExistsException e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}catch (Exception e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/
	
	/*@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String> create(
			@RequestBody CountryDTO dto
			){
		try {
			//Nivel 2 de validaciones
			Assert.notNull(dto, "Request es nulo");
			Assert.hasText(dto.getId(), "ID es nulo o vacío");
			Assert.hasText(dto.getName(), "Name es nulo o vacío");
			Assert.hasText(dto.getRegionName(), "RegionName es nulo o vacío");
			
			Assert.isTrue(dto.getId().length() == 2, "ID no tiene 2 carácteres");
			Assert.isTrue(dto.getName().length() >= 3, "Name debe tener mas de 2 caracteres");
			Assert.isTrue(dto.getRegionName().length() >= 3, "RegionName solo tiene 2 caracteres o menos");			
			
			String countryId = this.service.create(dto);
			
			return new ResponseEntity<>(countryId, HttpStatus.OK);														
		}catch (IllegalArgumentException e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}catch (EntityAlreadyExistsException e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}catch (Exception e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String> create(
			//Nivel 3 de validaciones
			@RequestBody @Valid CountryDTO dto
			){
		try {
			//Otras aserciones
			
			String countryId = this.service.create(dto);
			
			return new ResponseEntity<>(countryId, HttpStatus.OK);														
		}catch (IllegalArgumentException e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}catch (EntityAlreadyExistsException e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}catch (Exception e) {
			log.error("Error en create", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			value = "/id/{id}")
	public ResponseEntity<Void> modify(
			@PathVariable(name = "id") String id,
			
			@RequestBody @Valid CountryDTO dto
			) {
		try {			
			dto.setId(id);
			
			this.service.modify(dto); 
			
			return new ResponseEntity<>(HttpStatus.OK);
		}catch (EntityNotFoundException e) {
			log.error("Error en modify", e);
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e) {
			log.error("Error en modify", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping(value = "/id/{id}")
	public ResponseEntity<Void> deleteById(
			@PathVariable(value = "id") String id
			){
		try {			
			this.service.deleteById(id); 
			
			return new ResponseEntity<>(HttpStatus.OK);
		}catch (EntityNotFoundException e) {
			log.error("Error en modify", e);
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception e) {
			log.error("Error en modify", e);
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//Nivel 1 de validaciones
	private boolean validateCreateRequest(CountryDTO dto) {
		if(StringUtils.isNotBlank(dto.getId()) && 
		   StringUtils.isNotBlank(dto.getName()) &&
		   StringUtils.isNotBlank(dto.getRegionName()) &&
		   
		   dto.getId().length() == 2 &&
		   dto.getName().length() >= 3 &&
		   dto.getRegionName().length() >= 3) {
			return true;					
		}else {
			return false;
		}	
	}
	
	//200 Exitoso
	//400 Petición incorrecta
	//401 Autenticación fallida (*)
	//403 Autorización fallida (*)
	//500 Error interno
		
	//Crear API EmployeeAPI
	//Consultar todos los empleados --> GET
	//Consultar todos los empleados por nombre y apellido (coincidencia) --> GET Path
	//Consultar todos los empleados por departamento y/o cargo --> GET (Path o Query)
	
	//Crear empleado --> POST --> 200 Creo, 409 Ya existe el empleado por nombre y apellido
	//Actualizar empleado --> PUT --> ID por URL, el resto de la información por Payload -> 200 OK, 204 si no existe 
	//Borrar empleado --> DELETE  --> ID por URL, Debe verificar si el empleado por ID existe, si existe (no borro)
	//                    200 y si no existe retorno 204
	
	//Merge empleado  --> El empleado existe, lo actualiza, si el empleado no existe lo crea (nombre y apellido)
	// 201 Creo y 200 Actualizo
}
