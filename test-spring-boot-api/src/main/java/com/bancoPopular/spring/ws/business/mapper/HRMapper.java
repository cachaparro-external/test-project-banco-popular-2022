package com.bancoPopular.spring.ws.business.mapper;

import com.bancoPopular.spring.ws.dto.CountryDTO;
import com.bancoPopular.spring.ws.dto.RegionDTO;
import com.bancoPopular.spring.ws.persistent.entity.CountryEntity;
import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;

//@Mapper(componentModel = "spring")
public interface HRMapper {
	
	//Entity --> DTO 
	public RegionDTO convertFromRegionEntityToRegionDTO(RegionEntity entity);
	
	//@Mapping(target = "regionName", source = "region.name")
	public CountryDTO convertFromCountryEntityToCountryDTO(CountryEntity entity);
	
	//DTO --> Entity
	public RegionEntity convertFromRegionDTOToRegionEntity(RegionDTO dto);
	public CountryEntity convertFromCountryDTOToCountryEntity(CountryDTO dto);

}
