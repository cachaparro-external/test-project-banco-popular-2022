package com.bancoPopular.spring.ws.business.old;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;
import com.bancoPopular.spring.ws.persistent.repository.RegionRepository;

@Service
public class TransactionExample {
	
	@Autowired
	private RegionRepository regionRep;
	
	@Transactional
	public long deleteRegionByName(String name) {
		//long cantRegistrosBorrados = this.regionRep.deleteByName(name);
		List<RegionEntity> regiones = this.regionRep.removeByName(name);
		
		//return cantRegistrosBorrados;
		return regiones.size();
	}
	
	@Transactional()
	public void executeTransaction() {
		this.createRegion();
		
		//UPDATE
		Optional<RegionEntity> regionOpt = this.regionRep.findByName("Africa Norte");
		
		if(regionOpt.isPresent()) {
			RegionEntity regionU = regionOpt.get();
			
			regionU.setName("Africa Sur");
			
			this.regionRep.save(regionU);
			
			System.out.println("Se actualizo la región");
		}else {
			System.out.println("La región no existe");
		}
		
		//DELETE
		long cantRegistrosBorrados = this.regionRep.deleteByName("Americas");
		
		System.out.println("Cantidad de regiones borradas: " + cantRegistrosBorrados);
	}
	
	private void createRegion() {
		//INSERT
		RegionEntity region = new RegionEntity();
		region.setName("Oceania");
		
		region = this.regionRep.save(region);
		
		System.out.println("Se crear la región " + region.getName() + " con ID " + region.getId());
	}

}
