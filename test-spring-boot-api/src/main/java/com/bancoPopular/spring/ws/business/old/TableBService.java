package com.bancoPopular.spring.ws.business.old;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancoPopular.spring.ws.persistent.entity.TableBEntity;
import com.bancoPopular.spring.ws.persistent.repository.TableBRepository;

@Service
public class TableBService {
	
	@Autowired
	private TableBRepository tableBRep;

	public TableBEntity getById(Long id) {
		Optional<TableBEntity> tableBOpt = this.tableBRep.findById(id);
		
		if(tableBOpt.isPresent()) {
			System.out.println(tableBOpt.get());
			
			return tableBOpt.get();
		}else {
			return null;
		}
	}
	
}
