package com.bancoPopular.spring.ws.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bancoPopular.spring.ws.dto.EstudianteDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/banco-popular/estudiante")
@Log4j2

@Api(value = "API de estudiantes")
public class EstudianteAPI {
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/id/{idEstudiante}")
	
	@ApiOperation(value = "API para consultar estudiantes por ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "El estudiante existe y retorna la información"),
			@ApiResponse(code = 201, message = "El estudiante no existe")
	})
	public ResponseEntity<EstudianteDTO> findById(
			@ApiParam(required = true, value = "ID del estudiante")
			@PathVariable(value = "idEstudiante") Long id) {
		try {
			if(id <= 50) {
				EstudianteDTO dto = new EstudianteDTO();
				dto.setBirthDate(LocalDate.now());
				dto.setFirstName("Jose");
				dto.setLastName("Perez");
				dto.setId(id);
				
				return new ResponseEntity<EstudianteDTO>(dto, HttpStatus.OK);
			}else {
				return new ResponseEntity<EstudianteDTO>(HttpStatus.NO_CONTENT);
			}	
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<EstudianteDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/firstName/{firstName}/lastName/{lastName}")
	public ResponseEntity<List<EstudianteDTO>> findByFirstAndLastName(
			@PathVariable(value = "firstName") String firstName,
			@PathVariable(value = "lastName") String lastName
			) {
		try {									
			List<EstudianteDTO> estudiantes = this.getEstudiantes(firstName, lastName);
			
			return new ResponseEntity<List<EstudianteDTO>>(estudiantes, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<List<EstudianteDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/byNames/{firstName}/{lastName}")
	@ApiOperation(value = "Consulta de estudiantes por nombre y apellido")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Consulta exitosa")
	})
	public ResponseEntity<List<EstudianteDTO>> findByFirstAndLastName2(
			@ApiParam(required = true, value = "Primer nombre")
			@PathVariable(value = "firstName") String firstName,
			@ApiParam(required = false, value = "Apellido")
			@PathVariable(value = "lastName") String lastName
			) {
		try {									
			List<EstudianteDTO> estudiantes = this.getEstudiantes(firstName, lastName);
			
			return new ResponseEntity<List<EstudianteDTO>>(estudiantes, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<List<EstudianteDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<EstudianteDTO>> findByFirstAndLastName3(			
			@RequestParam(value = "firstName", required = true) String firstName,
			@RequestParam(value = "lastName", required = false, defaultValue = "Chaparro") String lastName
			) {
		try {									
			List<EstudianteDTO> estudiantes = this.getEstudiantes(firstName, lastName);
			
			return new ResponseEntity<List<EstudianteDTO>>(estudiantes, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<List<EstudianteDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/all")
	public ResponseEntity<List<EstudianteDTO>> getAll() {
		try {									
			List<EstudianteDTO> estudiantes = this.getEstudiantes("Rodrigo", "Gomez");
			
			return new ResponseEntity<List<EstudianteDTO>>(estudiantes, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en getAll", e);			
			
			return new ResponseEntity<List<EstudianteDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	//GET --> POST (restricción de 255)
	//1. Muchos filtros
	//2. Filtro cuyo valor es muy largo
	//3. Caracteres especiales: & = ? /
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
				 produces = {MediaType.APPLICATION_JSON_VALUE},
				 value = "/byFilter")
	@ApiIgnore
	public ResponseEntity<List<EstudianteDTO>> findByFirstAndLastName4(			
			@RequestBody EstudianteDTO requestDto
			) {
		try {									
			List<EstudianteDTO> estudiantes = this.getEstudiantes(requestDto.getFirstName(), requestDto.getLastName());
			
			return new ResponseEntity<List<EstudianteDTO>>(estudiantes, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<List<EstudianteDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Long> create(
			@RequestBody EstudianteDTO dto
			) {
		try {									
			Random random = new Random(System.currentTimeMillis());
			
			dto.setId((long)random.nextInt(100));
			
			if(dto.getId() <= 50) {
				return new ResponseEntity<Long>(dto.getId(), HttpStatus.OK);
			}else {
				return new ResponseEntity<Long>(HttpStatus.CONFLICT);
			}									
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE},
			value = "/2")
	public ResponseEntity<EstudianteDTO> create2(
			@RequestBody EstudianteDTO dto
			) {
		try {									
			Random random = new Random(System.currentTimeMillis());
			
			dto.setId(random.nextLong());
			
			return new ResponseEntity<EstudianteDTO>(dto, HttpStatus.OK);
			//return new ResponseEntity<EstudianteDTO>(dto, HttpStatus.CREATED);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<EstudianteDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		
	}
	
	@PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Void> modify(
			@RequestBody EstudianteDTO dto
			) {
		try {									
			if(dto.getId() != null) {
				//Proceso de actualización
				return new ResponseEntity<Void>(HttpStatus.OK);
			}else{
				return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
			}	
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
			value = "/id/{id}")
	public ResponseEntity<Void> modify2(
			@PathVariable(name = "id") Long id,			
			@RequestBody EstudianteDTO dto
			) {
		try {															
			log.info("Actualizando estudiante con ID " + id);
			
			Assert.notNull(id, "ID is null");
			
			//Proceso de actualización
			return new ResponseEntity<Void>(HttpStatus.OK);						
		}catch (IllegalArgumentException e) {
			log.warn(e.getMessage());
			
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@DeleteMapping(value = "/id/{id}")
	public ResponseEntity<Void> delete(
			@PathVariable(value = "id") Long id 
			) {
		try {															
			log.info("Borrando estudiante con ID " + id);
			
			//Proceso de borrado
			return new ResponseEntity<Void>(HttpStatus.OK);						
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@PostMapping(value = "/execute")
	public ResponseEntity<Void> ejecutarNomina(
			@RequestHeader(value = HttpHeaders.CONTENT_TYPE, required = false, defaultValue = MediaType.APPLICATION_JSON_VALUE) String contentType,
			@RequestHeader(value = "username", required = true) String username
			) {
		log.info("Ejecutando proceso para usuario " + username + " content type " + contentType);
		
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@PostMapping(value = "/execute/2")
	public ResponseEntity<Void> ejecutarNomina2() {
		log.info("Ejecutando proceso");
		
		MultiValueMap<String, String> headers = new HttpHeaders();
		headers.put("username", Arrays.asList("cachaparro"));
		headers.put(HttpHeaders.CONTENT_TYPE, Arrays.asList(MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE));
		
		return new ResponseEntity<Void>(headers, HttpStatus.OK);	
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			value = "/byNames/fn/{firstName}/ln/{lastName}")
	public ResponseEntity<List<EstudianteDTO>> findByFirstAndLastName4(
			@PathVariable(value = "firstName") String firstName,
			@PathVariable(value = "lastName") String lastName,
			
			@RequestHeader(value = HttpHeaders.ACCEPT) String acceptHeader 
			) {
		try {									
			log.info("Formato de respuesta: " + acceptHeader);
			
			List<EstudianteDTO> estudiantes = this.getEstudiantes(firstName, lastName);
			
			return new ResponseEntity<List<EstudianteDTO>>(estudiantes, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Error en findById", e);			
			
			return new ResponseEntity<List<EstudianteDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private List<EstudianteDTO> getEstudiantes(String firstName, String lastName){
		Random random = new Random(System.currentTimeMillis());
		
		List<EstudianteDTO> estudiantes = new ArrayList<>();
		
		EstudianteDTO dto = new EstudianteDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		dto.setId(random.nextLong());
		
		estudiantes.add(dto);
		
		dto = new EstudianteDTO();
		dto.setBirthDate(LocalDate.now());
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		dto.setId(random.nextLong());
		
		estudiantes.add(dto);
		
		return estudiantes;
	}
	
	//Pendiente:
	//Como responder con diferentes tipos de contenidos
	//Que una misma capacidad soporte multiples metodos HTTP
	
	//API para materias
	// ID (Long), nombre (String), cantidad de horas semanales (Integer), creditos (Integer), fecha de creación (LocalDateTime)
	// GET Consultar todas las asignaturas
	// GET Consultar por ID (PATH)
	// GET de consultar por nombre (Query)
	
	//POST --> Retorna el ID, Retorna todo la materia
	//PUT --> Recibir el ID por URL (path) o por payload
	//DELETE --> Recibe el ID por URL (path)

}
