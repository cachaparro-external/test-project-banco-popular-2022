package com.bancoPopular.spring.ws.business.mapper;

import com.bancoPopular.spring.ws.dto.A1;
import com.bancoPopular.spring.ws.dto.B1;
import com.bancoPopular.spring.ws.dto.TestADTO;
import com.bancoPopular.spring.ws.dto.TestBDTO;

//@Mapper(componentModel = "spring")
public interface TestMapper {
	
	//@Mapping(target = "idTest", source = "id")
	//@Mapping(target = "nameTest", source = "name")
	//@Mapping(target = "b1", source = "a1")
	public TestBDTO testADtoToTestBDto(TestADTO dto);

	//@Mapping(target = "firstName", expression = "java(new String(a1.getName()).split(\" \")[0])")
	//@Mapping(target = "lastName", expression = "java(new String(a1.getName()).split(\" \")[1])")
	public B1 fromA1ToB1(A1 a1);
	
	//@Mapping(target = "id", source = "idTest")
	//@Mapping(target = "name", source = "nameTest")
	//@Mapping(target = "a1", source = "b1")
	public TestADTO testBDtoToTestADto(TestBDTO dto);
	
	//@Mapping(target = "name", expression = "java(b1.getFirstName().concat(b1.getLastName()))")
	public A1 fromB1ToA1(B1 b1);
}
