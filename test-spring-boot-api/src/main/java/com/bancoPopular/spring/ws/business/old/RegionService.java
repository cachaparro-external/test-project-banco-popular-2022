package com.bancoPopular.spring.ws.business.old;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bancoPopular.spring.ws.persistent.entity.RegionEntity;
import com.bancoPopular.spring.ws.persistent.repository.Region2Repository;
import com.bancoPopular.spring.ws.persistent.repository.RegionRepository;

@Service
public class RegionService {
	
	@Autowired
	private RegionRepository regionRep;
	
	@Autowired
	private Region2Repository region2Rep;

	@Transactional
	public RegionEntity getById(Long id){
		Optional<RegionEntity> regionOpt = this.regionRep.findById(id);
		
		if(regionOpt.isPresent()) {
			System.out.println("Region ID " + regionOpt.get().getId());
			System.out.println("Region Name " + regionOpt.get().getName());
			
			/*regionOpt.get().getCountries().forEach((country) -> {
				System.out.println("Country: " + country.toString());
			});*/
			
			return regionOpt.get();
 		}else {
			return null;
		}
	}
	
	//@Transactional
	public void borrarPorNombre(String regionName) {
		//long cantRegBorrados = this.regionRep.deleteByName(regionName);
		int cantRegBorrados = this.regionRep.borrarPorNombre(regionName);
		
		System.out.println("Los registros borrados: " + cantRegBorrados);
	}
	
	@Transactional
	public Long create(String regionName) {
		RegionEntity region = new RegionEntity();
		region.setName(regionName);
		
		region = this.regionRep.save(region);
		
		System.out.println("Region: " + region);
		
		return region.getId();
	}
	
	@Transactional
	public Long createAndFlush(String regionName) {
		RegionEntity region = new RegionEntity();
		region.setName(regionName);
		
		region = this.region2Rep.saveAndFlush(region);
		
		System.out.println("Region: " + region);
		
		return region.getId();
	}
	
}
