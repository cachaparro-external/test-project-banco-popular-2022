package com.bancoPopular.spring.ws.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "regions")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionEntity {
	
	@Id
	@GeneratedValue(generator = "RegionEntityGenerator", strategy = GenerationType.IDENTITY)
	@Column(name = "region_id")
	private Long id;
	
	@Column(name = "region_name")
	private String name;
	
	/*@OneToMany(fetch = FetchType.LAZY, targetEntity = CountryEntity.class, mappedBy = "regionId")
	private List<CountryEntity> countries;*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
