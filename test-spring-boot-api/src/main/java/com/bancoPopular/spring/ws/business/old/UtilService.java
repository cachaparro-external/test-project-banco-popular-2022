package com.bancoPopular.spring.ws.business.old;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancoPopular.spring.ws.persistent.dao.UtilDAO;

@Service
public class UtilService {

	@Autowired
	private UtilDAO dao;
	
	public void createTable(String tableName) {
		this.dao.createTable(tableName);
	}
	
}
