package com.bancoPopular.spring.ws.business.converter;

import org.dozer.DozerConverter;

public class NameConverter extends DozerConverter<String, String> {

	public NameConverter() {
		super(String.class, String.class);
	}
	
	public NameConverter(Class<String> prototypeA, Class<String> prototypeB) {
		super(prototypeA, prototypeB);
	}

	@Override
	public String convertTo(String source, String destination) {
		return source.concat(" ").concat(destination);
	}

	@Override
	public String convertFrom(String source, String destination) {		
		return source.split(" ")[0];
	}

}
