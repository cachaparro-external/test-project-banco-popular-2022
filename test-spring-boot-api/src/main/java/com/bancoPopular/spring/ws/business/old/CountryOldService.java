package com.bancoPopular.spring.ws.business.old;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bancoPopular.spring.ws.persistent.entity.CountryEntity;
import com.bancoPopular.spring.ws.persistent.repository.CountryRepository;

@Service
public class CountryOldService {
	
	@Autowired
	private CountryRepository countryRep;
	
	@Transactional
	public CountryEntity getById(String id) {
		Optional<CountryEntity> countryOpt = this.countryRep.findById(id);
		
		if(countryOpt.isPresent()) {
			//System.out.println("Country: " + countryOpt.get());
			System.out.println("Country ID: " + countryOpt.get().getId());
			System.out.println("Country Name: " + countryOpt.get().getName());
			//System.out.println("Country Region Name: " + countryOpt.get().getRegion().getName());
			
			return countryOpt.get();
		}else {
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	public CountryEntity getById(String id, boolean includeRegion) {
		Optional<CountryEntity> countryOpt = this.countryRep.findById(id);
		
		if(countryOpt.isPresent()) {
			//System.out.println("Country: " + countryOpt.get());
			System.out.println("Country ID: " + countryOpt.get().getId());
			System.out.println("Country Name: " + countryOpt.get().getName());
			//System.out.println("Country Region Name: " + countryOpt.get().getRegion().getName());
			
			if(includeRegion) {
				System.out.println("Country Region Name: " + countryOpt.get().getRegion().getName());
			}
			
			return countryOpt.get();
		}else {
			return null;
		}
	}
	
	//@Transactional(readOnly = true)
	public List<CountryEntity> getByRegionName(String regionName){
		return this.countryRep.buscarPaisPorNombreRegionMejorada(regionName);
		//return this.countryRep.buscarPaisPorNombreRegion(regionName);
	}
	
	public List<Object[]> getByRegionName2(String regionName){
		return this.countryRep.buscarPaisPorNombreRegion2(regionName);
	}

}
