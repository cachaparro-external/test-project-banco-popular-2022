package com.bancoPopular.spring.ws.dto;

import lombok.Data;

@Data
public class B1 {

	private String firstName;
	private String lastName;
	private String country;
	
}
