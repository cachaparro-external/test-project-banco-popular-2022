package com.bancoPopular.test.maven;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.collections4.SetUtils.SetView;

public class TestMain {

	public static void main(String[] args) {
		System.out.println("Hola Mundo");
		
		Random random = new Random(System.currentTimeMillis());
		
		Set<Integer> numbers1 = new LinkedHashSet<Integer>();
		Set<Integer> numbers2 = new LinkedHashSet<Integer>();
		
		for(int i=0; i<10; i++) {
			numbers1.add(random.nextInt(100));
			numbers2.add(random.nextInt(100));
		}
		
		System.out.println("Numbers1: " + numbers1);
		System.out.println("Numbers2: " + numbers2);
		
		SetView<Integer> union = SetUtils.union(numbers1, numbers2);
		
		System.out.println("Union Numbers: " + union);
	}

}
