package com.bancoPopular.test.springBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication

@PropertySources({
	@PropertySource(ignoreResourceNotFound = false, value = "classpath:codes.yml")
})

//@Configuration
//@ComponentScan(basePackages = "com.bancoPopular.test.springBoot")
//@EnableAutoConfiguration
//@PropertySource("classpath:application.properties")
public class TestSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSpringBootApplication.class, args);
		System.out.println("Hello World from Spring Boot");
	}

}
