package com.bancoPopular.test.springBoot.beans;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class A {

	@Value("${spring.profiles.active}")
	private String profile;
	
	@Value("${username.1}")
	private String username;
	
	@Value("${banco-popular.rest.client.url}")
	private String url;
	
	@Autowired
	private B b;
	
	@PostConstruct
	public void init() {
		System.out.println("Init");
	}
	
	public void print() {
		System.out.println("Print from A");
		b.print();
	}
	
}
