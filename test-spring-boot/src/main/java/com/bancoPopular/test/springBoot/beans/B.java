package com.bancoPopular.test.springBoot.beans;

import org.springframework.stereotype.Component;

@Component("myBeanB")
public class B {

	public void print() {
		System.out.println("Print from B");
	}
	
}
